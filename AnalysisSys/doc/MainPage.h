/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** \mainpage notitle
 *  \anchor analysissysdoxygenmain
 *
 * This is the code reference manual for the Physics Analysis classes.

 * These pages have been generated directly from the code and reflect the exact
 * state of the software for this version of the AnalysisSys packages. More
 * information is available from the
 * <a href="http://cern.ch/lhcb-release-area/DOC/davinci/">web pages</a>
 * of the DaVinci project
 *
 * \sa
 * \li \ref lhcbdoxygenmain  "LHCbSys documentation"
 * \li \ref gaudidoxygenmain "Gaudi documentation"
 * \li \ref externaldocs     "Related external libraries"
 * \li <a href="https://twiki.cern.ch/twiki/bin/view/LHCb/DaVinci">DaVinci wiki page</a>

 * <hr>
 * \htmlinclude new_release.notes
 * <hr>
 * <b>Additional information:</b>
 * \li <a href="../release.notes"><b>Release notes history</b></a>
 * \li <a href="http://cern.ch/lhcb-release-area/DOC/analysis/"><b>Analysis project Web pages</b></a><p>
 */
