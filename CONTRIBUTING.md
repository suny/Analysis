# General information

In addition to `master`, this project contains several officialy maintained branches, with names such as `XXX-patches`.
They are all protected, meaning that code cannot be pushed into them directly but only through merge requests (MRs).
This helps with the validation of code prior to making it available in the official branches for future releases.

## Available supported branches

- `master` branch: new developments and updates targeting run 3. Builds on current supported platforms against latest version of Gaudi

- `run2-patches` branch: new developments and updates targeting runs 1+2 analysis and/or restripping. Builds on current supported platforms against latest version of Gaudi

- `2018-patches` branch: for 2018 incremental stripping (`S34r0pX`, `S35r0pX`, `S35r1pX`), 2015 and 2016 restripping (`S24r2`, `S28r2`) and patches to stripping in 2015, 2016 and 2018 simulation workflows. Builds with gcc62 on slc6 and centos7 and with gcc7 on centos7

- `2017-patches` branch: for 2017 incremental stripping (`S29r2pX` (pp), `S32r0pX` (pp 5 Tev)) and patches to stripping (`S29r2`, `S29r2p1`, `S32`) in 2017 simulation workflows. Builds with gcc49 on slc6 and with gcc62 on slc6 and centos7

- `2016-patches` branch: for 2016 incremental stripping (`S28r1pX`, `S30r2pX`, `S30r3pX`) and patches to stripping in 2016 simulation workflows. Builds with gcc49 on slc6

- `stripping24-patches` branch: for 2015 incremental stripping and patches to stripping in 2015 simulation workflows. Builds with gcc49 on slc6

- `stripping21-patches` branch: for run 1 incremental stripping (`S21r0pX`, `S21r1pX`) and patches to stripping in run 1 simulation workflows. Builds with gcc49 on slc6


## Where to commit code to

- Bug fixes specific to a given processing should be committed to the corresponding `XXX-patches` branch.

- Any changes or fixes for Run 1 and Run 2 analysis (or re-stripping) should go to the `run2-patches` branch.
  They will then be propagated to `master` (if relevant also for Upgrade) by the applications managers. 
  Things may be different for bug fixes to old and/or specific versions of DaVinci, in which case it is probably best to discuss unless you know exactly what you are doing.

- Any changes specific to Upgrade should *only* go to `master`

In doubt, please get in touch before creating a MR.
