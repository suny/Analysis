/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLTRIGGERRECOSTATS_H
#define TUPLETOOLTRIGGERRECOSTATS_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IEventTupleTool.h" // Interface

/** @class TupleToolTriggerRecoStats TupleToolTriggerRecoStats.h
 *
 *  Counts the number of particles per type
 *
 *  WIP: Does this have a use?
 *
 *  @author Patrick Koppenburg
 *  @date   2009-07-30
 */
class TupleToolTriggerRecoStats : public TupleToolBase, virtual public IEventTupleTool {
public:
  /// Standard constructor
  TupleToolTriggerRecoStats( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~TupleToolTriggerRecoStats();       ///< Destructor
  StatusCode fill( Tuples::Tuple& ) override; ///< Fill tuple

protected:
private:
  std::vector<std::string> m_locations; ///< Hlt locations to look at
};
#endif // TUPLETOOLTRIGGERRECOSTATS_H
