/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "boost/regex.hpp"

#include "Event/Particle.h"
// kernel
#include "Event/HltDecReports.h"
#include "Kernel/IANNSvc.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
// local
#include "TupleToolTriggerBase.h"

//#include <sstream>

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolTriggerBase
//
// 2008-04-09 : V. Gligorov
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolTriggerBase::TupleToolTriggerBase( const std::string& type, const std::string& name, const IInterface* parent )
    : TupleToolBase( type, name, parent ), m_hlt1( 0 ), m_hlt2( 0 ), m_triggerList( 0 ) {
  // declareInterface<IParticleTupleTool>(this);

  // to turn all verbosity on, set Verbose=true
  declareProperty( "VerboseHlt1", m_verboseHlt1 = false );
  declareProperty( "VerboseHlt2", m_verboseHlt2 = false );
  declareProperty( "FillHlt1", m_doHlt1 = true );
  declareProperty( "FillHlt2", m_doHlt2 = true );

  // List of triggers to look at
  declareProperty( "TriggerList", m_triggerList = std::vector<std::string>( 0 ) );
}

//=============================================================================
// Destructor
//=============================================================================
TupleToolTriggerBase::~TupleToolTriggerBase() {}

//=============================================================================

//=========================================================================
//  initialize
//=========================================================================
StatusCode TupleToolTriggerBase::initialize() {
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  if ( isVerbose() ) m_verboseHlt1 = m_verboseHlt2 = true;
  const bool iv = ( m_verboseHlt1 || m_verboseHlt2 );

  if ( !m_triggerList.empty() && !iv ) {
    warning()
        << "You have set a list of triggers to look for, but not asked for verbose mode ... OK, but this is weird! "
        << endmsg;
  }

  if ( m_triggerList.size() == 0 && iv ) {
    warning() << "You have not set a list of triggers to look for, but have asked for verbose output. " << endmsg;
    m_verboseHlt1 = m_verboseHlt2 = false;
  }

  // bug, missing this line
  if ( !m_triggerList.empty() ) compileMyList( m_triggerList );

  return sc;
}

//=========================================================================
//  Fill
//=========================================================================

StatusCode TupleToolTriggerBase::fill( const LHCb::Particle* M, const LHCb::Particle* P, const std::string& head,
                                       Tuples::Tuple& tuple ) {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "fill particle " << P << endmsg;

  bool test = true;
  test &= fillBasic( M, P, head, tuple );

  // Fill details about the requested triggers
  if ( m_verboseHlt1 || m_verboseHlt2 || isVerbose() ) test &= fillVerbose( M, P, head, tuple );

  return StatusCode( test );
}

StatusCode TupleToolTriggerBase::fill( Tuples::Tuple& tuple ) {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "fill tuple " << endmsg;
  bool test = true;
  test &= fillBasic( tuple );

  // Fill details about the requested triggers
  if ( m_verboseHlt1 || m_verboseHlt2 || isVerbose() ) test &= fillVerbose( tuple );

  return StatusCode( test );
}

bool TupleToolTriggerBase::compileMyList( const std::vector<std::string>& list ) {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "compiling List " << endmsg;

  boost::regex hlt1( "Hlt1.*Decision" );
  boost::regex hlt2( "Hlt2.*Decision" );

  for ( std::vector<std::string>::const_iterator s = list.begin(); s != list.end(); ++s ) {
    if ( boost::regex_match( *s, hlt1 ) ) {
      m_hlt1.push_back( *s );
    } else if ( boost::regex_match( *s, hlt2 ) ) {
      m_hlt2.push_back( *s );
    } else {
      error() << "List member ``" << *s
              << "'' does not match any known pattern. Have you forgotten the trailing 'Decision' ?" << endmsg;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << " ==== HLT1 ==== " << endmsg;
    for ( std::vector<std::string>::const_iterator s = m_hlt1.begin(); s != m_hlt1.end(); ++s )
      debug() << " " << ( *s );
    debug() << endmsg;
    debug() << " ==== HLT2 ==== " << endmsg;
    for ( std::vector<std::string>::const_iterator s = m_hlt2.begin(); s != m_hlt2.end(); ++s )
      debug() << " " << ( *s );
    debug() << endmsg;
    debug() << " ==== Compiled list ====" << endmsg;
  }

  return true;
}

StatusCode TupleToolTriggerBase::fillBasic( const LHCb::Particle*, const LHCb::Particle*, const std::string&,
                                            Tuples::Tuple& ) {
  return Error( "Don't call the fill of the baseclass!!" );
}

StatusCode TupleToolTriggerBase::fillBasic( Tuples::Tuple& ) {
  return Error( "Don't call the fill of the baseclass!!" );
}

StatusCode TupleToolTriggerBase::fillVerbose( const LHCb::Particle*, const LHCb::Particle*, const std::string&,
                                              Tuples::Tuple& ) {
  return Error( "Don't call the fill of the baseclass!!" );
}

StatusCode TupleToolTriggerBase::fillVerbose( Tuples::Tuple& ) {
  return Error( "Don't call the fill of the baseclass!!" );
}
