/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "Kernel/ILHCbMagnetSvc.h"

// local
#include "TupleToolEventInfo.h"

#include "Event/HltDecReport.h"
#include "Event/HltDecReports.h"
#include "Event/ODIN.h" // event & run number

#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "Event/VertexBase.h"
#include "GaudiAlg/ITupleTool.h"
#include "GaudiAlg/Tuple.h"

#include "GaudiKernel/IRegistry.h" // IOpaqueAddress

//-----------------------------------------------------------------------------
// Implementation file for class : EventInfoTupleTool
//
// 2007-11-07 : Jeremie Borel
//-----------------------------------------------------------------------------

using namespace Gaudi;
using namespace LHCb;

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolEventInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolEventInfo::TupleToolEventInfo( const std::string& type, const std::string& name, const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  declareInterface<IEventTupleTool>( this );
}

//=============================================================================

StatusCode TupleToolEventInfo::initialize() {
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;
  m_magSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );
  return sc;
}

//=============================================================================
StatusCode TupleToolEventInfo::fill( Tuples::Tuple& tuple ) {
  const std::string prefix = fullName();

  // Load the ODIN
  const LHCb::ODIN* odin = m_odinDh.get();
  if ( !odin ) {
    // should always be available ...
    return Error( "Cannot load the ODIN data object", StatusCode::SUCCESS );
  }

  LHCb::HltDecReports* decreport_hlt1 = m_hlt1DecReportDh.getIfExists();
  LHCb::HltDecReports* decreport_hlt2 = m_hlt2DecReportDh.getIfExists();
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    debug() << "Getting Hlt1 and Hlt2 reports " << decreport_hlt1 << " " << decreport_hlt2 << endmsg;
  }
  if ( !decreport_hlt1 ) Warning( "No Hlt1 DecReport found. Are you sure?", StatusCode::SUCCESS, 1 ).ignore();
  if ( !decreport_hlt2 ) Warning( "No Hlt2 DecReport found. Are you sure?", StatusCode::SUCCESS, 1 ).ignore();

  double chrono_time = 0;
  if ( !m_algNames.empty() ) {
    for ( const auto& name : m_algNames ) {
      const auto alg_time = chronoSvc()->chronoDelta( name + ":Execute", IChronoStatSvc::ELAPSED ); // microseconds
      chrono_time += alg_time;
      if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) ) verbose() << name << " " << alg_time << endmsg;
    }
    if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) ) verbose() << "TOTAL: " << chrono_time << endmsg;
  } else {
    if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) ) {
      verbose() << "No algorithms to time, skipping the addition of chronoDelta branch!" << endmsg;
    }
  }

  bool test = true;

  // Fill the tuple

  test &= tuple->column( prefix + "runNumber", odin->runNumber() );
  test &= tuple->column( prefix + "eventNumber", odin->eventNumber() );
  test &= tuple->column( prefix + "BCID", odin->bunchId() );
  test &= tuple->column( prefix + "BCType", odin->bunchCrossingType() );
  test &= tuple->column( prefix + "OdinTCK", odin->triggerConfigurationKey() );
  test &= tuple->column( prefix + "HLT1TCK", decreport_hlt1 ? decreport_hlt1->configuredTCK() : 0 );
  test &= tuple->column( prefix + "HLT2TCK", decreport_hlt2 ? decreport_hlt2->configuredTCK() : 0 );
  test &= tuple->column( prefix + "GpsTime", odin->gpsTime() );

  if ( !m_algNames.empty() ) { test &= tuple->column( prefix + "chronoDelta", chrono_time ); }

  if ( isVerbose() ) {
    const Gaudi::Time gtime = odin->eventTime();
    test &= tuple->column( prefix + "GpsYear", gtime.year( false ) );
    test &= tuple->column( prefix + "GpsMonth", gtime.month( false ) );
    test &= tuple->column( prefix + "GpsDay", gtime.day( false ) );
    test &= tuple->column( prefix + "GpsHour", gtime.hour( false ) );
    test &= tuple->column( prefix + "GpsMinute", gtime.minute( false ) );
    test &= tuple->column( prefix + "GpsSecond", gtime.second( false ) + ( gtime.nsecond() / 1e9 ) );
    test &= tuple->column( prefix + "TriggerType", odin->triggerType() );
  }

  test &= tuple->column( prefix + "Polarity", (short)( m_magSvc->isDown() ? -1 : 1 ) );

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Returns " << test << endmsg;
  return StatusCode( test );
}
