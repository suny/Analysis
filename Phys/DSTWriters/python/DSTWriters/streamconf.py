###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''Classes and functions to control the configuration of output streams used for DST writing. '''

from builtins import str
from builtins import object
__author__ = "Juan PALACIOS juan.palacios@nikhef.nl"

__all__ = ('OutputStreamConf')

from copy import copy


class OutputStreamConf(object):
    def __init__(self,
                 streamType=None,
                 filePrefix='Sel',
                 fileExtension='.dst',
                 extraItems=[],
                 vetoItems=[],
                 selectiveRawEvent=False,
                 remapTES=None,
                 killTESAddressHistory=True):
        self.streamType = streamType
        self.filePrefix = filePrefix
        self.extension = fileExtension
        self.extraItems = copy(extraItems)
        self.vetoItems = vetoItems
        self.selectiveRawEvent = selectiveRawEvent
        self.remapTES = remapTES
        self.killTESAddressHistory = killTESAddressHistory
        self.name = ''

    def __str__(self):
        output = '\nOutputStreamConfXX\n'
        output += 'name       : ' + self.name + '\n'
        output += 'streamType : ' + str(self.streamType) + '\n'
        output += 'filePrefix : ' + str(self.filePrefix) + '\n'
        output += 'extension  : ' + str(self.extension) + '\n'
        output += 'extraItems : ' + str(self.extraItems) + '\n'
        output += 'selectiveRawEvent : ' + str(self.selectiveRawEvent) + '\n'
        output += 'killTESAddressHistory : ' + str(
            self.killTESAddressHistory) + '\n'
        return output
