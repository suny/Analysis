###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Default configuration parameters for selection DST.
"""

__author__ = "Juan Palacios <juan.palacios@nikhef.nl>"

__all__ = ('selDSTElements', 'selDSTStreamConf')

from Configurables import InputCopyStream
from DSTWriters.streamconf import OutputStreamConf
from DSTWriters.microdstelements import CloneParticleTrees, ClonePVRelations


def selDSTElements():
    vetoTESList = [
        "/Event/Rec/Vertex/Primary", "/Event/Rec/ProtoP/Charged",
        "/Event/Rec/ProtoP/Neutrals", "/Event/Rec/Track/Best",
        "/Event/Rec/Track/FittedHLT1VeloTracks", "/Event/Rec/Rich/PIDs",
        "/Event/Rec/Track/Muon", "/Event/Rec/Muon/MuonPID",
        "/Event/Rec/Calo/Electrons"
        #"/Event/Rec/Calo/Photons",
        #"/Event/Rec/Calo/MergedPi0s",
        #"/Event/Rec/Calo/SplitPhotons"
    ]
    alwaysClone = ["/Event/Rec/ProtoP/Neutrals"]
    return [
        CloneParticleTrees(
            TESVetoList=vetoTESList, TESAlwaysClone=alwaysClone),
        ClonePVRelations(
            location="Particle2VertexRelations",
            clonePVs=True,
            TESVetoList=vetoTESList)
    ]


def selDSTStreamConf():
    return OutputStreamConf(streamType=InputCopyStream)
