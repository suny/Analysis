/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef P2MCP_P2MCPFUNCTORS_H
#define P2MCP_P2MCPFUNCTORS_H 1

// Include files
#include "LoKi/BuildMCTrees.h"
#include "P2MCP/P2MCPTypes.h"
#include <algorithm>
#include <functional>
/** @namespace P2MCP::Functors P2MCP/P2MCPFunctors.h
 *
 *
 *  @author Juan Palacios
 *  @date   2009-03-13
 */
namespace P2MCP {
  namespace Functors {

    struct SortInTrees : public std::binary_function<const LHCb::MCParticle*, const LHCb::MCParticle*, bool> {

      bool operator()( const LHCb::MCParticle* p1, const LHCb::MCParticle* p2 ) const {
        LoKi::MCParticles::FromMCDecayTree fromDecay( p1 );
        return fromDecay( p2 );
      }
    };

    struct InTree : public std::unary_function<const LHCb::MCParticle*, bool> {

      InTree( const LHCb::MCParticle* head ) : m_head( head ) {}

      bool operator()( const LHCb::MCParticle* mc ) const {
        P2MCP::Functors::SortInTrees sorter;
        return sorter( m_head, mc );
      }

    private:
      InTree() {}

    private:
      const LHCb::MCParticle* m_head;
    };

    template <class Selector, class Sorter>
    struct SortIntoTrees {

      template <class InputContainer>
      P2MCP::Types::FlatTrees operator()( const InputContainer& mcps ) const {
        LHCb::MCParticle::ConstVector heads = LoKi::MCTrees::buildTrees( mcps );

        P2MCP::Types::FlatTrees output;

        LHCb::MCParticle::ConstVector::iterator itHead = heads.begin();

        for ( ; itHead != heads.end(); ++itHead ) {
          Selector               selector( *itHead );
          P2MCP::Types::FlatTree tree;
          std::copy_if( mcps.begin(), mcps.end(), std::back_inserter( tree ), std::cref( selector ) );
          std::stable_sort( tree.begin(), tree.end(), Sorter() );
          output.push_back( tree );
        }
        return output;
      }
    };

  } // namespace Functors

} // namespace P2MCP
#endif // P2MCP_P2MCPFUNCTORS_H
