/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLMCPVASSOCIATION_H
#define TUPLETOOLMCPVASSOCIATION_H 1

// Include files
// from Gaudi
#include <DecayTreeTupleBase/TupleToolBase.h>
#include <Gaudi/Property.h>
#include <Kernel/IParticleTupleTool.h> // Interface

/** @class TupleToolMCPVAssociation TupleToolMCPVAssociation.h
 *
 * Ask if the head is association to the correct PV
 * Adapted from TupleToolMCKinematic
 * See the properties inline documentation to toggle on/off some of
 * the columns
 *
 * - head_MCPV_[XYZ] : the position of the MC PV of the MC particle associated to the candidate.
 * - head_RecPVToMCPV_Chi2 : the chi2 of the reco'd PV wrt the MC PV.
 * - head_RecPV_correct : whether the candidate's best PV is associated to the correct MC PV.
 * - head_RecPV_nMatchTracks : the number of tracks in the candidate's best PV that're associated to the MC PV.
 * - head_RecPV_matchFrac : the fraction of tracks in the candidate's best PV that're associated to the MC PV.
 *
 * Note that the PV2MC tool that's used to do the PV associations requires the Track -> MCParticle association
 * table to be in the format produced by LoKi::Track2MC, so you'll need to run an instance of this in your
 * sequence before the DecayTreeTuple. Also, for run 2, the tracks used to fit the PVs are contained in
 * Rec/Track/FittedHLT1VeloTracks, so you'll need to add this to the Tracks attribute of LoKi::Track2MC.
 *
 * \sa TupleToolPV2MC, DecayTreeTuple, MCDecayTreeTuple, PV2MC, Track2MC
 *
 *  @author Michael Alexander & Adam Davis
 *  @date   2018-05-09
 */

struct IDVAlgorithm;
class IParticle2MCAssociator;
class IPV2MC;

class TupleToolMCPVAssociation : public TupleToolBase, virtual public IParticleTupleTool {
public:
  /// Standard constructor
  TupleToolMCPVAssociation( const std::string& type, const std::string& name, const IInterface* parent );

  virtual StatusCode initialize() override;
  virtual StatusCode fill( const LHCb::Particle*, const LHCb::Particle*, const std::string&, Tuples::Tuple& ) override;

private:
  Gaudi::Property<std::vector<std::string>> m_p2mcAssocTypes{
      this,
      "IP2MCPAssociatorTypes",
      {"DaVinciSmartAssociator", "MCMatchObjP2MCRelator"},
      "Types of particle -> MC particle associators to be used (in order)."};
  std::vector<IParticle2MCAssociator*> m_p2mcAssocs;
  Gaudi::Property<std::string>         m_pv2mctooltype{this, "PV2MCToolType", "LoKi::PV2MC",
                                               "Type of PV2MC tool to be used (must inherit from IPV2MC)"};
  IPV2MC*                              m_pv2mctool = nullptr;
  IDVAlgorithm*                        m_dva       = nullptr;
};
#endif // TUPLETOOLMCPVASSOCIATION_H
