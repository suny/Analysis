/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCTUPLETOOLP2VV_H
#define MCTUPLETOOLP2VV_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IMCParticleTupleTool.h" // Interface

/** @class MCTupleToolP2VV MCTupleToolP2VV.h
 *
 *
 *  Fill MC Particle
 *
 * See the properties inline documentation to toggle on/off some of
 * the columns
 *
 * - head_XXXX :
 *
 * \sa TupleToolMCTruth, DecayTreeTuple
 *
 *  @author Patrick Koppenburg
 *  @date   2009-01-19
 */
class IP2VVMCPartAngleCalculator;
class MCTupleToolP2VV : public TupleToolBase, virtual public IMCParticleTupleTool {
public:
  /// Standard constructor
  MCTupleToolP2VV( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~MCTupleToolP2VV(); ///< Destructor
  StatusCode initialize() override;
  StatusCode fill( const LHCb::MCParticle*, const LHCb::MCParticle*, const std::string&, Tuples::Tuple& ) override;

private:
  IP2VVMCPartAngleCalculator* m_angleTool;
  std::string                 m_calculator; //!< Name of the tool for the angles calculation
};
#endif // MCTUPLETOOLP2VV_H
