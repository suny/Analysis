/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCTUPLETOOLPID_H
#define MCTUPLETOOLPID_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Event/MCParticle.h"            // Interface
#include "Kernel/IMCParticleTupleTool.h" // Interface

/** @class MCTupleToolPID MCTupleToolPID.h
 *
 *  Fill MC Particle
 *
 * - head_ID : pid
 *
 *
 * \sa MCTupleToolPID, DecayTreeTuple, MCDecayTreeTuple
 *
 *  @author Rob Lambert
 *  @date   2009-11-19
 */

class MCTupleToolPID : public TupleToolBase, virtual public IMCParticleTupleTool {

public:
  /// Standard constructor
  MCTupleToolPID( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~MCTupleToolPID(); ///< Destructor

  StatusCode fill( const LHCb::MCParticle*, const LHCb::MCParticle*, const std::string&, Tuples::Tuple& ) override;
};

#endif // MCTUPLETOOLPID_H
