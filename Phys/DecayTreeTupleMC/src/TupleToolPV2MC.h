/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLPV2MC_H
#define TUPLETOOLPV2MC_H 1

// Include files
// from Gaudi
#include <DecayTreeTupleBase/TupleToolBase.h>
#include <Event/RecVertex.h>
#include <Gaudi/Property.h>
#include <Kernel/IEventTupleTool.h> // Interface

class IPV2MC;

/** @class TupleToolPV2MC TupleToolPV2MC.h
 *
 * Write the relations table of PV -> MC PV. Should be used in conjunction with TupleToolPrimaries and
 * MCTupleToolPrimaries. Fills:
 *
 * - nPVs : Number of reco'd PVs in the container.
 * - RecPV_nMatches : Array, one entry per reco'd PV. The number of MC PVs the PV has been checked against (normally
 * just the number of MC PVs in the event).
 * - RecPV_totMatchNTracks : Array, as above. The total number of tracks in each PV that're matched to an MC particle.
 * - RecPV_totMatchFrac : Array, as above. The total fraction of tracks in each PV that're matched to an MC particle.
 * - RecPV_matchMCPVIndices : Matrix, one row per reco'd PV, each row i is of length RecPV_nMatches[i]. The sorted lists
 * of the indices of the best matching MC PVs in the MC PVs container. The sorting is descending by the number of tracks
 * matched to each PV.
 * - RecPV_matchNTracks : Matrix as above. The number of tracks in the PV matched to each MC PV.
 * - RecPV_matchFracs : Matrix as above. The fraction of the tracks matched to each MC PV.
 * - RecPV_matchChi2s : Matrix as above. The chi2 of the PV wrt the position of each MC PV.
 * - RecPV_matchDistances : Matrix as above. The distances between the PV and each MC PV.
 *
 * Example usage:
 *
 * - Select PVs with at least 50 % of tracks originating from the same MC PV with "RecPV_matchFracs[][0] >= 0.5".
 * - Plot the chi2 of PVs wrt their best matching MC PV with "RecPV_matchChi2s[][0]" and selection as above.
 * - Plot the PV x resolution with "PVX[] - MCPVX[RecPV_matchMCPVIndices[][0]]" and selection as above, assuming
 * TupleToolPrimaries and MCTupleToolPrimaries have been used.
 *
 * Note that the PV2MC tool that's used to do the PV associations requires the Track -> MCParticle association
 * table to be in the format produced by LoKi::Track2MC, so you'll need to run an instance of this in your
 * sequence before the DecayTreeTuple. Also, for run 2, the tracks used to fit the PVs are contained in
 * Rec/Track/FittedHLT1VeloTracks, so you'll need to add this to the Tracks attribute of LoKi::Track2MC.
 *
 * \sa TupleToolPrimaries, MCTupleToolPrimaries, TupleToolMCPVAssociation, PV2MC, Track2MC.
 *  @author Michael Alexander
 *  @date   2018-05-09
 */
class TupleToolPV2MC : public TupleToolBase, virtual public IEventTupleTool {
public:
  /// Standard constructor
  TupleToolPV2MC( const std::string& type, const std::string& name, const IInterface* parent );

  virtual StatusCode fill( Tuples::Tuple& ) override;
  virtual StatusCode initialize() override;

private:
  IPV2MC*                      m_pv2mc = nullptr;
  Gaudi::Property<std::string> m_pv2mctooltype{this, "PV2MCToolType", "LoKi::PV2MC",
                                               "Type of PV2MC tool to be used (must inherit from IPV2MC)"};
  Gaudi::Property<std::string> m_pvslocation{this, "PVLocation", LHCb::RecVertexLocation::Primary,
                                             "Location of PVs to associate."};
};
#endif // TUPLETOOLPV2MC_H
