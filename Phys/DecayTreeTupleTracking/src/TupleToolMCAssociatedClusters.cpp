/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "TupleToolMCAssociatedClusters.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/MCParticle.h"
#include "Event/Particle.h"

// from Event/LinkerEvent
#include "Linker/LinkedFrom.h"
#include "Linker/LinkedFromKey.h"
#include "Linker/LinkedTo.h"

// from Event/TrackEvent
#include "Event/FTCluster.h"
#include "Event/Track.h"
#include "Event/UTCluster.h"
#include "Event/VPCluster.h"

// from Event/MCEvent
#include "Event/MCParticle.h"

// kernel
#include "Kernel/IParticle2MCAssociator.h"

TupleToolMCAssociatedClusters::TupleToolMCAssociatedClusters( const std::string& type, const std::string& name,
                                                              const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  // interface
  declareInterface<IParticleTupleTool>( this );

  // MC associators to try, in order
  m_p2mcAssocTypes.push_back( "DaVinciSmartAssociator" );
  m_p2mcAssocTypes.push_back( "MCMatchObjP2MCRelator" );
  declareProperty( "IP2MCPAssociatorTypes", m_p2mcAssocTypes );

  declareProperty( "CheckUniqueness", m_checkUniqueness = true );
}

//=============================================================================
// Initialization. Check parameters
//=============================================================================
StatusCode TupleToolMCAssociatedClusters::initialize() {
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  // the MC associators
  m_p2mcAssocs.clear();
  for ( std::vector<std::string>::const_iterator iMCAss = m_p2mcAssocTypes.begin(); iMCAss != m_p2mcAssocTypes.end();
        ++iMCAss ) {
    m_p2mcAssocs.push_back( tool<IParticle2MCAssociator>( *iMCAss, this ) );
  }
  if ( m_p2mcAssocs.empty() ) { return Error( "No MC associators configured" ); }

  return StatusCode::SUCCESS;
}

StatusCode TupleToolMCAssociatedClusters::fill( const LHCb::Particle*, const LHCb::Particle* P, const std::string& head,
                                                Tuples::Tuple& tuple ) {
  // retrieve the currently linked MCP, this will be our working base
  const LHCb::MCParticle* mcp( NULL );
  const std::string       prefix = fullName( head );

  unsigned int nFoundFT = 0;
  unsigned int nFoundUT = 0;
  unsigned int nFoundVP = 0;

  unsigned int nFoundUniqueFT = 0;
  unsigned int nFoundUniqueUT = 0;
  unsigned int nFoundUniqueVP = 0;

  size_t nMCHitsFT = 0;
  size_t nMCHitsUT = 0;
  size_t nMCHitsVP = 0;

  if ( !P ) return StatusCode::FAILURE;

  // first just return if the particle isn't supposed to have a track
  if ( !P->isBasicParticle() ) return StatusCode::SUCCESS;

  const auto& protop = P->proto();
  if ( !protop ) return StatusCode::SUCCESS;

  const auto& track = protop->track();
  if ( !track ) return StatusCode::SUCCESS;

  const auto& lhcbIDs = track->lhcbIDs();

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Number of lhcbIDs: " << lhcbIDs.size() << endmsg;

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Getting related MCP to " << P << endmsg;

  for ( std::vector<IParticle2MCAssociator*>::const_iterator iMCAss = m_p2mcAssocs.begin();
        iMCAss != m_p2mcAssocs.end(); ++iMCAss ) {
    mcp = ( *iMCAss )->relatedMCP( P );
    if ( mcp ) break;
  }

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Got mcp " << mcp << endmsg;

  if ( mcp != NULL ) {
    // get VPClusters and count correct and total number of clusters
    // Get the linker table MCParticle => VPCluster
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Trying to retrieve linker tables." << endmsg;

    LinkedFromKey<LHCb::MCParticle, LHCb::FTChannelID> ftLink( evtSvc(), msgSvc(), LHCb::FTClusterLocation::Default );

    LinkedTo<LHCb::MCParticle, LHCb::UTCluster>   utLink( evtSvc(), msgSvc(), LHCb::UTClusterLocation::UTClusters );
    LinkedFrom<LHCb::UTCluster, LHCb::MCParticle> utLinkRev( evtSvc(), msgSvc(), LHCb::UTClusterLocation::UTClusters );

    LinkedFrom<LHCb::VPCluster, LHCb::MCParticle> vp2MCPLink( evtSvc(), msgSvc(), LHCb::VPClusterLocation::Default );
    LinkedTo<LHCb::MCParticle, LHCb::VPCluster>   MCP2VPLink( evtSvc(), msgSvc(), LHCb::VPClusterLocation::Default );

    if ( ftLink.notFound() ) return Error( "Unable to retrieve MCParticle-Cluster linker table for FT" );

    if ( utLink.notFound() ) return Error( "Unable to retrieve MCParticle-Cluster linker table for UT" );

    if ( vp2MCPLink.notFound() ) return Error( "Unable to retrieve MCParticle-VPCluster linker table" );

    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Building cluster lists for this MCP." << endmsg;

    const auto& ftChannelIdList = ftLink.keyRange( mcp );
    const auto& utClusterList   = utLinkRev.range( mcp );
    const auto& vpClusterList   = vp2MCPLink.range( mcp );

    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Counting cluster lists." << endmsg;
    nMCHitsFT = ftChannelIdList.size();
    nMCHitsUT = utClusterList.size();
    nMCHitsVP = vpClusterList.size();

    /**
     * Comparing here the number of LHCb IDs v.s. the number of MCParticle clusters.
     */
    std::vector<int> usedFTIDs;
    std::vector<int> usedUTIDs;
    std::vector<int> usedVPIDs;

    for ( const auto& lhcbID : lhcbIDs ) {
      if ( lhcbID.isFT() ) {
        const LHCb::FTChannelID& measuredFTChannelID = lhcbID.ftID();
        const auto&              it                  = std::find_if( ftChannelIdList.begin(), ftChannelIdList.end(),
                                       [&measuredFTChannelID]( const LHCb::FTChannelID& ftchannelid ) {
                                         return ftchannelid == measuredFTChannelID;
                                       } );

        if ( it != ftChannelIdList.end() ) {
          if ( m_checkUniqueness &&
               std::find( usedFTIDs.begin(), usedFTIDs.end(), measuredFTChannelID ) == usedFTIDs.end() ) {
            usedFTIDs.push_back( measuredFTChannelID );
            nFoundUniqueFT++;
          }

          nFoundFT++;
        }
      }

      if ( lhcbID.isUT() ) {
        const auto& measuredUTChannelID = lhcbID.stID();
        const auto& it                  = std::find_if( utClusterList.begin(), utClusterList.end(),
                                       [&measuredUTChannelID]( const LHCb::UTCluster* const& cluster ) {
                                         return cluster->channelID() == measuredUTChannelID;
                                       } );

        if ( it != utClusterList.end() ) {
          if ( m_checkUniqueness &&
               std::find( usedUTIDs.begin(), usedUTIDs.end(), measuredUTChannelID ) == usedUTIDs.end() ) {
            usedUTIDs.push_back( measuredUTChannelID );
            nFoundUniqueUT++;
          }

          nFoundUT++;
        }
      }

      if ( lhcbID.isVP() ) {
        const auto& measuredVPChannelID = lhcbID.vpID();
        const auto& it                  = std::find_if( vpClusterList.begin(), vpClusterList.end(),
                                       [&measuredVPChannelID]( const LHCb::VPCluster* const& cluster ) {
                                         return cluster->channelID() == measuredVPChannelID;
                                       } );

        if ( it != vpClusterList.end() ) {
          if ( m_checkUniqueness &&
               std::find( usedVPIDs.begin(), usedVPIDs.end(), measuredVPChannelID ) == usedVPIDs.end() ) {
            usedVPIDs.push_back( measuredVPChannelID );
            nFoundUniqueVP++;
          }

          nFoundVP++;
        }
      }
    }
  }

  tuple->column( prefix + "_MC_nUsedFTClusters", nFoundFT ).ignore();
  tuple->column( prefix + "_MC_nUsedUTClusters", nFoundUT ).ignore();
  tuple->column( prefix + "_MC_nUsedVPClusters", nFoundVP ).ignore();

  if ( m_checkUniqueness ) {
    tuple->column( prefix + "_MC_nUsedUniqueFTClusters", nFoundUniqueFT ).ignore();
    tuple->column( prefix + "_MC_nUsedUniqueUTClusters", nFoundUniqueUT ).ignore();
    tuple->column( prefix + "_MC_nUsedUniqueVPClusters", nFoundUniqueVP ).ignore();
  }

  tuple->column( prefix + "_MC_nFTClusters", nMCHitsFT ).ignore();
  tuple->column( prefix + "_MC_nUTClusters", nMCHitsUT ).ignore();
  tuple->column( prefix + "_MC_nVPClusters", nMCHitsVP ).ignore();

  return StatusCode::SUCCESS;
}

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolMCAssociatedClusters )
