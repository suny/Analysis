/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
// local
#include "Event/ChiSquare.h"
#include "Event/FitNode.h"
#include "Event/KalmanFitResult.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "TrackKernel/TrackFunctors.h"
#include "TrackKernel/TrackTraj.h"
#include "TupleToolTrackTime.h"

#include <boost/lexical_cast.hpp>

#include "Event/FitNode.h"
#include "Event/Measurement.h"
#include "Event/Track.h"
#include "LHCbMath/ValueWithError.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolTrackTime
//
// 2016-02-26 : Francesco Dettori
//              francesco.dettori@cern.ch
//
// Based on code from W. Hulsbergen
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolTrackTime )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolTrackTime::TupleToolTrackTime( const std::string& type, const std::string& name, const IInterface* parent )
    : TupleToolBase( type, name, parent ), m_trackfitter( "TrackMasterFitter", this ) {
  declareInterface<IParticleTupleTool>( this );
  declareProperty( "TrackFitter", m_trackfitter );
}

//=============================================================================
// Destructor
//=============================================================================
TupleToolTrackTime::~TupleToolTrackTime() {}

StatusCode TupleToolTrackTime::initialize() {
  StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  sc = m_trackfitter.retrieve();

  return sc;
}

//=============================================================================
StatusCode TupleToolTrackTime::fill( const LHCb::Particle*, const LHCb::Particle* P, const std::string& head,
                                     Tuples::Tuple& tuple ) {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << " filling particle " << endmsg;
  const std::string prefix = fullName( head );
  bool              test   = true;

  if ( !P ) return StatusCode::FAILURE;

  // first just return if the particle isn't supposed to have a track
  if ( !P->isBasicParticle() ) return StatusCode::SUCCESS;

  const LHCb::ProtoParticle* protop = P->proto();
  if ( !protop ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Protoparticle not found for this particle" << endmsg;
    return StatusCode::SUCCESS;
  }

  const LHCb::Track* track = protop->track();
  if ( !track ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Track not found for this particle" << endmsg;
    return StatusCode::SUCCESS;
  }

  if ( msgLevel( MSG::DEBUG ) )
    debug() << prefix << " " << track->type() << " " + prefix + "_TRACK_CHI2 " << track->chi2() << endmsg;
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << *track << endmsg;

  Gaudi::Math::ValueWithError track_t0( 0, 0 );
  try {
    LHCb::Track* clone = new LHCb::Track( *track, track->key() );

    StatusCode sc = m_trackfitter->operator()( *clone );
    if ( sc.isSuccess() ) {
      track_t0 = computeTrackT0( *clone );
      if ( msgLevel( MSG::DEBUG ) ) debug() << " Obtained t0: " << track_t0 << endmsg;
    } else {
      Warning( "Unable to refit track: track time will have default value 0" ).ignore();
    }
    delete clone;

  } catch ( const GaudiException& ) { Warning( "There was a problem accessing data for track refit" ).ignore(); }

  // First method
  test &= tuple->column( prefix + "_TRACK_time", track_t0.value() );
  test &= tuple->column( prefix + "_TRACK_time_err", track_t0.error() );

  return StatusCode::SUCCESS;
}

Gaudi::Math::ValueWithError TupleToolTrackTime::computeTrackT0( const LHCb::Track& ) {

  // Check if fit result is available (this should always be the case)
  Gaudi::Math::ValueWithError rc( 0, 0 );

  return rc;
}
