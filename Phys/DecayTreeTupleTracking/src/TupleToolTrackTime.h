/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DecayTreeTupleTracking_TupleToolTrackTime_H
#define DecayTreeTupleTracking_TupleToolTrackTime_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/IParticleTupleTool.h" // Interface
#include "LHCbMath/ValueWithError.h"
#include "TrackInterfaces/ITrackFitter.h"

/** @class TupleToolTrackTime TupleToolTrackTime.h
 *  \brief TupleTool to calculate timing information on the track
 *
 *  @author Francesco Dettori
 *  @date   2016-26-02
 */

class TupleToolTrackTime : public TupleToolBase, virtual public IParticleTupleTool {

public:
  /// Standard constructor
  TupleToolTrackTime( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~TupleToolTrackTime(); ///< Destructor

  StatusCode initialize() override;

public:
  StatusCode fill( const LHCb::Particle*, const LHCb::Particle*, const std::string&, Tuples::Tuple& ) override;

private:
  Gaudi::Math::ValueWithError computeTrackT0( const LHCb::Track& track );

private:
  ToolHandle<ITrackFitter> m_trackfitter;
};

#endif // DecayTreeTupleTracking_TupleToolTrackTime_H
