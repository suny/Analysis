###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# @brief: provides a GaudiSequencer with a track momentum scaler and an update algorithm for the candidate particles on TES.
# @author: Paul Seyfert
# @date: 2013-02-21
# idea mainly by V. Belyaev

from __future__ import print_function
import six


def ParticleRefitterSeq(inputs=[], rootInTES="/Event", scale=True):
    from Configurables import GaudiSequencer
    from Configurables import TrackScaleState as SCALER
    from Configurables import ParticleRefitter
    scaler = SCALER("Scaler", RootInTES=rootInTES)
    seq = GaudiSequencer("ParticleRefitterSeq")
    if scale:
        seq.Members = [scaler]
    else:
        seq.Members = []
    refitter = ParticleRefitter()
    if isinstance(inputs, six.string_types):
        refitter.Inputs = [inputs]
    else:
        refitter.Inputs = inputs
    refitter.RootInTES = rootInTES
    print("ParticleRefitterSeq is applied to the following inputs:")
    for i in refitter.Inputs:
        print("   - on of the inputs is ", i)
    seq.Members += [refitter]
    return seq
