/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INCLUDE_PARTICLEREFITTER_H
#define INCLUDE_PARTICLEREFITTER_H 1

#include "Kernel/DaVinciAlgorithm.h"

struct IParticle2State;
struct IVertexFit;

/**
 * @class ParticleRefitter ParticleRefitter.h
 * @brief update LHCb::Particles to collect updates to their LHCb::Track objects
 * mainly copied from preliminary work by Christian Voss
 *
 * @author Paul Seyfert <pseyfert@cern.ch>
 * @date   2013-02-19
 */
class ParticleRefitter : public DaVinciAlgorithm {

public:
  /// Standard Constructor
  ParticleRefitter( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm event execution
  //  virtual StatusCode   finalize(); ///< Algorithm finalize

protected:
  virtual StatusCode execute( LHCb::Particle* );

private:
  IParticle2State*           m_stateToParticle;
  std::string                m_trktoolname, m_fittoolname;
  IVertexFit*                m_fit;
  std::vector<LHCb::Vertex*> m_vt; // @todo: keeping vertices like this in the storage isn't exactly good practice (use
                                   // TES instead)
};
#endif // INCLUDE_PARTICLEREFITTER_H
