/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TracksFromParticles.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "LoKi/PhysExtract.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TracksFromParticles
//
// 2012-10-27 : Matthew Needham
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( TracksFromParticles )

TracksFromParticles::TracksFromParticles( const std::string& name, ISvcLocator* pSvc )
    : DaVinciAlgorithm( name, pSvc ) {
  declareProperty( "inputLocation", m_outputLocation = "/Event/Rec/MyTracks" );
  ///@fixme: "inputLocation" as property for m_outputLocation ???
}

StatusCode TracksFromParticles::execute() {

  // get the input particles
  const LHCb::Particle::Range inputParticles = particles();
  std::vector<LHCb::Track*>   tracks;
  LoKi::Extract::getTracks( inputParticles.begin(), inputParticles.end(), std::back_inserter( tracks ) );

  if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) ) { verbose() << " Found  " << tracks.size() << endmsg; }

  // now clone the tracks and output
  LHCb::Tracks* outputCont = new LHCb::Tracks();
  for ( auto track : tracks ) {
    if ( !outputCont->object( track->key() ) ) {
      // clone track only if it's not a duplicate from another candidate
      LHCb::Track* newTrack = new LHCb::Track( *track, track->key() );
      outputCont->insert( newTrack );
    }
  }

  put( outputCont, m_outputLocation );

  setFilterPassed( true );

  return StatusCode::SUCCESS;
}
