/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef JBOREL_TUPLETOOLDECAY_H
#define JBOREL_TUPLETOOLDECAY_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
//#include "Event/Particle.h"
//#include "Kernel/IParticleTupleTool.h"
#include "DecayTreeTupleBase/ITupleToolDecay.h"

/** @class TupleToolDecay TupleToolDecay.h jborel/TupleToolDecay.h
 *
 * \brief Not really meant to be used outside DecayTreeTuple
 *
 * look at this later doc for more information.
 *
 * It has two properties:
 *
 * - \b ToolList, a vector of string stating which tool are associated
 to this part of the decay
 *
 * - \b InheritTools, boolean, stating whether the tools given in the
 parent DecayTreeTuple should be inherit to this part of the decay.
 *
 * \sa DecayTreeTuple
 *
 *  @author Jeremie Borel
 *  @date   2007-11-02
 */
class TupleToolDecay : public GaudiTool, virtual public ITupleToolDecay {

public:
  /// Standard constructor
  TupleToolDecay( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~TupleToolDecay(); ///< Destructor

  StatusCode initializeDecay( const std::string&, bool ) override;

public:
  std::string decay() const override;

  std::string getInfo() const override;
  void        printInfo() const override;

  IDecayFinder*   decayFinder() const override;
  IMCDecayFinder* mcDecayFinder() const override;

  bool                           useLoKiDecayFinders() const override { return m_useLoKiDecayFinders; }
  const Decays::IMCDecay::iTree& mcDecayTree() const override { return m_mcdecayTree; }
  const Decays::IDecay::iTree&   decayTree() const override { return m_decayTree; }

  bool hasMatched() const;
  void hasMatched( bool state );

  bool inheritTools() const override;

  const std::string&              getName() const override;
  const std::vector<std::string>& getStuffers() const override;

private:
  bool isMC() const { return m_isMC; }

private:
  bool m_hasMatched;
  bool m_inheritTools;

  std::string m_myName;

  std::vector<std::string> m_stufferList;

  IDecayFinder*   m_dkFinder;
  IMCDecayFinder* m_mcdkFinder;
  bool            m_isMC;

  bool                   m_useLoKiDecayFinders;
  Decays::IMCDecay::Tree m_mcdecayTree; ///< MC truth decay tree
  Decays::IDecay::Tree   m_decayTree;   ///< MC truth decay tree
};

#endif // JBOREL_TUPLETOOLDECAY_H
