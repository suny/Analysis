/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FILTERMCPARTICLEARRAYBYDECAY_H
#define FILTERMCPARTICLEARRAYBYDECAY_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IMCParticleArrayFilter.h" // Interface

/** @class FilterMCParticleArrayByDecay FilterMCParticleArrayByDecay.h
 *
 *
 *
 *  @author Juan Palacios
 *  @date   2007-07-25
 */

struct IMCDecayFinder;

class FilterMCParticleArrayByDecay : public GaudiTool, virtual public IMCParticleArrayFilter {
public:
  /// Standard constructor
  FilterMCParticleArrayByDecay( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;
  StatusCode finalize() override;

  /// Filter and put results into new array
  StatusCode filter( const LHCb::MCParticle::ConstVector&, LHCb::MCParticle::ConstVector& ) const override;
  /// Filter and remove elements that do not pass filter from array
  StatusCode filter( LHCb::MCParticle::ConstVector& ) const override;

protected:
private:
  void findDecayHeads( const LHCb::MCParticle::ConstVector& in, LHCb::MCParticle::ConstVector& heads ) const;
  void findAllDecay( const LHCb::MCParticle::ConstVector& heads, LHCb::MCParticle::ConstVector& decay ) const;

private:
  std::string     m_decayFinderName;
  IMCDecayFinder* m_decayFinder;
};
#endif // FILTERMCPARTICLEARRAYBYDECAY_H
