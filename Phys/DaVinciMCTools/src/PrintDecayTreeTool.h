/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRINTDECAYTREETOOL_H
#define PRINTDECAYTREETOOL_H 1

#include <map>
#include <set>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from DaVinciTools
#include "Kernel/IPrintDecay.h"
#include "Kernel/IPrintDecayTreeTool.h"

#include "boost/format.hpp"
#include "boost/lexical_cast.hpp"

// Forward declarations
namespace LHCb {
  class IParticlePropertySvc;
}

class MsgStream;

/** @class PrintDecayTreeTool PrintDecayTreeTool.h
 *
 *  This is an implementation of the  IPrintDecayTreeTool.
 *  It is based on Olivier Dormond's DebugTool.
 *
 *  In the following function a maxDepth is provided with a default value
 *  of -1. That way the tool "PrintDepth" property is used.
 *  @todo write documentation stating what the options do.
 *
 *  @author Olivier Dormond
 *  @author Juan Palacios juancho@nikhef.nl
 *  @date   10/10/2007
 */
class PrintDecayTreeTool final : public GaudiTool, virtual public IPrintDecay, virtual public IPrintDecayTreeTool {

public:
  /// Standard Constructor
  PrintDecayTreeTool( const std::string& type, const std::string& name, const IInterface* parent );

  /// Destructor
  ~PrintDecayTreeTool() = default; ///< Destructor

  StatusCode initialize( void ) override;

  /**
   *   Print decay tree for a given particle.
   *   Here, a maxDepth is provided with a default value
   *   of -1. The default uses the value set by the
   *   "PrintDepth" property.
   */
  void printTree( const LHCb::Particle* mother, int maxDepth = -1 ) const override;

  void printTree( const LHCb::MCParticle* mother, Particle2MCLinker* assoc, int maxDepth = -1 ) const override;

  void printTree( const LHCb::Particle* mother, Particle2MCLinker* assoc, int maxDepth = -1 ) const override;

  void printAsTree( const LHCb::MCParticle::ConstVector& particles, Particle2MCLinker* assoc ) const override;

  void printAsTree( const LHCb::MCParticles& particles, Particle2MCLinker* assoc ) const override;

  void printAsList( const LHCb::Particle::ConstVector& particles ) const override;

  void printAsList( const LHCb::Particle::ConstVector& particles, Particle2MCLinker* assoc ) const override;

  void printAsList( const LHCb::MCParticle::ConstVector& particles, Particle2MCLinker* assoc ) const override;

  void printAsList( const LHCb::Particles& particles ) const override;

  void printAsList( const LHCb::Particles& particles, Particle2MCLinker* assoc ) const override;

  void printAsList( const LHCb::MCParticles& particles, Particle2MCLinker* assoc ) const override;

private:
  /// Information Types
  enum InfoKeys { Name, E, M, P, Pt, Px, Py, Pz, Vx, Vy, Vz, theta, phi, eta, idcl, chi2, PK, PPK };

private:
  void printHeader( MsgStream& log, bool mcfirst, bool associated ) const;

  std::set<std::string> printInfo( const std::string& prefix, const LHCb::MCParticle* part, Particle2MCLinker* assoc,
                                   MsgStream& log ) const;

  std::set<std::string> printInfo( const std::string& prefix, const LHCb::Particle* part, Particle2MCLinker* assoc,
                                   MsgStream& log ) const;

  std::set<std::string> printDecayTree( const LHCb::MCParticle* mother, Particle2MCLinker* assoc,
                                        const std::string& prefix, int depth, MsgStream& log ) const;

  std::set<std::string> printDecayTree( const LHCb::Particle* mother, Particle2MCLinker* assoc,
                                        const std::string& prefix, int depth, MsgStream& log ) const;

  void printUsedContainers( MsgStream& log, std::set<std::string> containers ) const;

  /// Get TES location for an object
  template <class TYPE>
  inline std::string tesLocation( const TYPE obj ) const {
    return ( obj && obj->parent() && obj->parent()->registry() ? obj->parent()->registry()->identifier() : "NotInTES" );
  }

  inline unsigned int tesCode( const std::string& loc ) const {
    static std::map<std::string, unsigned int> tesLocs;
    static unsigned int                        lastTESCode( 0 );
    if ( tesLocs.find( loc ) == tesLocs.end() ) { tesLocs[loc] = lastTESCode++; }
    return tesLocs[loc];
  }

private:
  LHCb::IParticlePropertySvc* m_ppSvc = nullptr;       ///< Reference to particle property service
  int                         m_depth;                 ///< Depth of printing for tree
  int                         m_treeWidth;             ///< width of the tree drawing
  int                         m_fWidth;                ///< width of the data fields
  int                         m_fPrecision;            ///< precision of the data fields
  std::string                 m_arrow;                 ///< arrow drawing
  std::string                 m_information;           ///< The specification of the values to print
  std::vector<InfoKeys>       m_keys{0};               ///< The list of information to print
  double                      m_energyUnit;            /// Unit for energies, momenta and masses
  double                      m_lengthUnit;            /// Unit for distances
  std::string                 m_energyUnitName{"MeV"}; ///< Unit for energies, momenta and masses
  std::string                 m_lengthUnitName{"mm"};  ///< Unit for distances
};

#endif // DEBUGTOOL_H
