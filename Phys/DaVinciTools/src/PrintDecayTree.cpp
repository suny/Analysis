/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files

#include "Event/Particle.h"
#include "GaudiAlg/Consumer.h"
#include "Kernel/IPrintDecay.h"
// ============================================================================
/** @class PrintDecayTree
 *  The simplified version of the algorithm PrintTree,
 *  which deals only with the reconstructed particles
 *  @see PrintTree
 *  @see IPrintDecay
 *  @author Vanya BELYAEV Ivan.Belayev@cern.ch
 *  @date 2008-03-30
 *  Adapted to Gaudi functional
 *  @author Patrick Koppenburg
 *  @date 2020-12-17
 */
class PrintDecayTree : public Gaudi::Functional::Consumer<void( const LHCb::Particles& )> {
public:
  // ==========================================================================
  /** the standard constructor
   *  @param name algorithm instance name
   *  @param pSvc service locator
   */
  PrintDecayTree( const std::string& name, ISvcLocator* pSvc ) : Consumer( name, pSvc, {KeyValue{"Input", ""}} ) {}
  // ==========================================================================
  /// the standard execution of the algorithm
  void operator()( const LHCb::Particles& parts ) const override {
    // get the tool
    m_printDecay->printTree( parts.begin(), parts.end(), -1 );
    if ( !parts.empty() ) {
      m_eventCount++;
      m_candidateCount += parts.size();
    }
    return;
  }

private:
  // ==========================================================================
  /// the IPrintDecay tool itself
  ToolHandle<IPrintDecay> m_printDecay = {this, "PrintDecayTreeTool", "PrintDecayTreeTool"};
  // counters
  mutable Gaudi::Accumulators::Counter<> m_eventCount{this, "Events"};
  mutable Gaudi::Accumulators::Counter<> m_candidateCount{this, "Candidates"};

  // ==========================================================================
};
// ============================================================================
/// declare the factory (needed for instantiation)
DECLARE_COMPONENT( PrintDecayTree )
// ============================================================================
// The END
// ============================================================================
