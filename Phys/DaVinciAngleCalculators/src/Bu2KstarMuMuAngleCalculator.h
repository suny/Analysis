/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef BU2KSTARMUMUANGLECALCULATOR_H
#define BU2KSTARMUMUANGLECALCULATOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IP2VVPartAngleCalculator.h" // Interface

/** @class Bu2KstarMuMuAngleCalculator Bu2KstarMuMuAngleCalculator.h
 *
 *  Calculates the three angles in a \f$ B^{+} \rightarrow K^{0}_{S} \pi^{+} \mu^{+} \mu^{-} \f$ and
 *  \f$ B^{+} \rightarrow K^{+} \pi^{0} \mu^{+} \mu^{-} \f$ decay.
 *  Angles are given in both the helicity and transversity bases.
 *
 *
 *  The Helicity Basis for \f$ B^{+} \rightarrow K^{*+}(892) \mu^{+} \mu^{-} \f$
 *  is defined by three angles \f$ \theta_{L} \f$, \f$ \theta_{K} \f$ and \f$ \phi \f$.
 *
 *  These angles are defined as:
 *
 *  \f$ \theta_{L} \f$ as the angle between the \f$ {\mu^{+}}(\mu^{-}) \f$ and the direction
 *  opposite the \f$ {B^{+}}({B^{-})} \f$ in the rest frame of the \f$ {\mu^{+}\mu^{-}} \f$.
 *  Equivalently this is the angle between the \f$ {\mu^{+}} \f$ in the \f$ {\mu^{+}\mu^{-}} \f$ rest
 *  frame and the direction of the \f$ {\mu^{+}\mu^{-}} \f$ in the B rest-frame.
 *
 *  \f$ \theta_{K} \f$ as the angle between the \f$ K \f$ in the \f$ {K^{*}} \f$ frame and
 *  the \f$ K^{*} \f$ in the B rest-frame.
 *
 *  \f$ \phi \f$ is defined in the B rest-frame as the angle between the planes defined by the
 *  \f$ {\mu_{+}}(\mu_{-}) \f$ and the \f$ K \pi \f$.
 *
 *
 *  @author Thomas Blake, David Gerick
 *  @date   2017-12-07
 */

class Bu2KstarMuMuAngleCalculator : public GaudiTool, virtual public IP2VVPartAngleCalculator {

public:
  /// Standard constructor
  Bu2KstarMuMuAngleCalculator( const std::string& type, const std::string& name, const IInterface* parent );

  ~Bu2KstarMuMuAngleCalculator(); ///< Destructor

  double calculateThetaL( const LHCb::Particle* particle ) override;
  double calculateThetaK( const LHCb::Particle* particle ) override;
  double calculatePhi( const LHCb::Particle* particle ) override;

  double calculateTransThetaTr( const LHCb::Particle* particle ) override;
  double calculateTransPhiTr( const LHCb::Particle* particle ) override;
  double calculateTransThetaV( const LHCb::Particle* particle ) override;

  StatusCode calculateAngles( const LHCb::Particle* particle, double& thetaL, double& thetaK, double& phi ) override;

  StatusCode calculateTransversityAngles( const LHCb::Particle* particle, double& Theta_tr, double& Phi_tr,
                                          double& Theta_V ) override;

  StatusCode initialize() override;

protected:
  /// Get daughters from B+
  StatusCode daughters( const LHCb::Particle* mother );

private:
  IParticleDescendants* m_descendants;

  const LHCb::Particle* m_pMuMinus;
  const LHCb::Particle* m_pMuPlus;
  const LHCb::Particle* m_pPi;
  const LHCb::Particle* m_pK;
};
#endif // BU2KSTARMUMUANGLECALCULATOR_H
