/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCBU2KSTARMUMUANGLECALCULATOR_H
#define MCBU2KSTARMUMUANGLECALCULATOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IP2VVMCPartAngleCalculator.h" // Interface
#include "Kernel/IParticlePropertySvc.h"
#include "MCInterfaces/IMCDecayFinder.h"

/** @class MCBu2KstarMuMuAngleCalculator MCBu2KstarMuMuAngleCalculator.h v1r3/MCBu2KstarMuMuAngleCalculator.h

 *
 *  Calculates the three angles in a \f$ B^{+} \rightarrow K^{0}_{S} \pi^{+} \mu^{+} \mu^{-} \f$ and
 *  \f$ B^{+} \rightarrow K^{+} \pi^{0} \mu^{+} \mu^{-} \f$ decay.
 *  Angles are given in both the helicity and transversity bases.
 *
 *
 *  The Helicity Basis for \f$ B^{+} \rightarrow K^{*+}(892) \mu^{+} \mu^{-} \f$
 *  is defined by three angles \f$ \theta_{L} \f$, \f$ \theta_{K} \f$ and \f$ \phi \f$.
 *
 *  These angles are defined as:
 *
 *  \f$ \theta_{L} \f$ as the angle between the \f$ {\mu^{+}}(\mu^{-}) \f$ and the direction
 *  opposite the \f$ {B^{+}}({B^{-})} \f$ in the rest frame of the \f$ {\mu^{+}\mu^{-}} \f$.
 *  Equivalently this is the angle between the \f$ {\mu^{+}} \f$ in the \f$ {\mu^{+}\mu^{-}} \f$ rest
 *  frame and the direction of the \f$ {\mu^{+}\mu^{-}} \f$ in the B rest-frame.
 *
 *  \f$ \theta_{K} \f$ as the angle between the \f$ K \f$ in the \f$ {K^{*}} \f$ frame and
 *  the \f$ K^{*} \f$ in the B rest-frame.
 *
 *  \f$ \phi \f$ is defined in the B rest-frame as the angle between the planes defined by the
 *  \f$ {\mu_{+}}(\mu_{-}) \f$ and the \f$ K \pi \f$.
 *
 *
 *
 *  @author Thomas Blake, Greig Cowan, David Gerick
 *  @date   2017-12-07
 */

class MCBu2KstarMuMuAngleCalculator : public GaudiTool, virtual public IP2VVMCPartAngleCalculator {

public:
  /// Standard constructor
  MCBu2KstarMuMuAngleCalculator( const std::string& type, const std::string& name, const IInterface* parent );

  ~MCBu2KstarMuMuAngleCalculator(); ///< Destructor

  StatusCode calculateAngles( const LHCb::MCParticle* particle, double& thetal, double& thetak, double& phi ) override;

  double calculateThetaL( const LHCb::MCParticle* particle ) override;
  double calculateThetaK( const LHCb::MCParticle* particle ) override;
  double calculatePhi( const LHCb::MCParticle* particle ) override;

  StatusCode calculateTransversityAngles( const LHCb::MCParticle* particle, double& Theta_tr, double& Phi_tr,
                                          double& Theta_phi_tr ) override;

  double calculateTransThetaTr( const LHCb::MCParticle* particle ) override;
  double calculateTransPhiTr( const LHCb::MCParticle* particle ) override;
  double calculateTransThetaV( const LHCb::MCParticle* particle ) override;

  double calculateMass( const LHCb::MCParticle* particle ) override;

  StatusCode initialize() override;

protected:
  StatusCode daughters( const LHCb::MCParticle* mother );
  bool       hasMother( const LHCb::MCParticle* particle, const std::vector<unsigned int>& ids ) const;

private:
  const LHCb::MCParticle* m_pMuPlus;
  const LHCb::MCParticle* m_pMuMinus;
  const LHCb::MCParticle* m_pK;
  const LHCb::MCParticle* m_pPi;

  IMCDecayFinder*             m_mcDecayFinder;
  LHCb::IParticlePropertySvc* m_ppSvc;

  std::vector<std::string> m_MuOrigin;
  std::vector<std::string> m_KPiOrigin;

  std::vector<unsigned int> m_MuOriginID;
  std::vector<unsigned int> m_KPiOriginID;
};

#endif // MCBU2KSTARMUMUANGLECALCULATOR_H
