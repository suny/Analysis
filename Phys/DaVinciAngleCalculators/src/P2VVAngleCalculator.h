/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef B2LLXANGLETOOL_H
#define B2LLXANGLETOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IP2VVAngleCalculator.h" // Interface

/** @class P2VVAngleCalculator P2VVAngleCalculator.h
 *
 *  Implementation of low-level tool that calculates the three angles between the four
 *  decay products of a P->VV decay. Use IP2VVPartAngleCalculator
 *  or IP2VVMCPartAngleCalculator instead.
 *
 *
 *  @author Thomas Blake, Greig Cowan
 *  @date   2007-08-22
 */

class P2VVAngleCalculator : public GaudiTool, virtual public IP2VVAngleCalculator {

public:
  /// Standard constructor
  P2VVAngleCalculator( const std::string& type, const std::string& name, const IInterface* parent );

  ~P2VVAngleCalculator(); ///< Destructor

  /// calculate the angle between two planes in the rest frame of the mother particle
  double calculatePlaneAngle( const Gaudi::LorentzVector&, const Gaudi::LorentzVector&, const Gaudi::LorentzVector&,
                              const Gaudi::LorentzVector&, const Gaudi::LorentzVector& ) override;
  /// calculate the polar angle in the rest frame of the mother particle
  double calculatePolarAngle( const Gaudi::LorentzVector&, const Gaudi::LorentzVector&,
                              const Gaudi::LorentzVector& ) override;

  double calculateThetaTr( const Gaudi::LorentzVector&, const Gaudi::LorentzVector&, const Gaudi::LorentzVector&,
                           const Gaudi::LorentzVector& ) override;

  double calculatePhiTr( const Gaudi::LorentzVector&, const Gaudi::LorentzVector&, const Gaudi::LorentzVector&,
                         const Gaudi::LorentzVector& ) override;
};
#endif // B2LLXANGLETOOL_H
