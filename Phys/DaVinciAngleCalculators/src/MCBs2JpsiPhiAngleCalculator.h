/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCANGLECALCULATOR_H
#define MCANGLECALCULATOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IP2VVMCPartAngleCalculator.h" // Interface

/** @class MCBs2JpsiPhiAngleCalculator MCBs2JpsiPhiAngleCalculator.h v1r3/MCBs2JpsiPhiAngleCalculator.h
 *
 *  Calculates helicity and transversity angles for a true B->J/psiPhi decay
 *
 *  @author Greig Cowan
 *  @date   2008-05-12
 */
class MCBs2JpsiPhiAngleCalculator : public GaudiTool, virtual public IP2VVMCPartAngleCalculator {

public:
  /// Standard constructor
  MCBs2JpsiPhiAngleCalculator( const std::string& type, const std::string& name, const IInterface* parent );

  ~MCBs2JpsiPhiAngleCalculator(); ///< Destructor

  StatusCode calculateAngles( const LHCb::MCParticle* particle, double& thetal, double& thetak, double& phi ) override;

  double calculateThetaL( const LHCb::MCParticle* particle ) override;
  double calculateThetaK( const LHCb::MCParticle* particle ) override;
  double calculatePhi( const LHCb::MCParticle* particle ) override;

  StatusCode calculateTransversityAngles( const LHCb::MCParticle* particle, double& Theta_tr, double& Phi_tr,
                                          double& Theta_V ) override;

  double calculateTransThetaTr( const LHCb::MCParticle* particle ) override;
  double calculateTransPhiTr( const LHCb::MCParticle* particle ) override;
  double calculateTransThetaV( const LHCb::MCParticle* particle ) override;

  double calculateMass( const LHCb::MCParticle* particle ) override;

protected:
  void                  fillDescendants( const LHCb::MCParticle*, LHCb::MCParticle::ConstVector&, int );
  IP2VVAngleCalculator* m_angle;

private:
  int m_depth;

private:
  StatusCode getParticles( const LHCb::MCParticle*&, LHCb::MCParticle::ConstVector&, const LHCb::MCParticle*&,
                           const LHCb::MCParticle*&, const LHCb::MCParticle*&, const LHCb::MCParticle*& );
};
#endif // MCANGLECALCULATOR_H
