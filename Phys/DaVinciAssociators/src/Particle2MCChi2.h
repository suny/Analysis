/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef Particle2MCChi2_H
#define Particle2MCChi2_H 1

// Include files
// from STL
#include <memory>
#include <string>

// from Gaudi
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/SmartDataPtr.h"

// local
#include "AsctAlgorithm.h"
#include "Kernel/Particle2MCLinker.h"

/** @class Particle2MCChi2 Particle2MCChi2.h
 *
 *
 *  @author Philippe Charpentier
 *  @date   17/05/2002
 */
class Particle2MCChi2 : public AsctAlgorithm {
public:
  /// Standard constructor
  Particle2MCChi2( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~Particle2MCChi2(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
private:
  double                             m_chi2;     ///< Chi2 maximum
  std::unique_ptr<Object2MCLinker<>> m_p2MCLink; ///< Pointer to a P2MCPLink object
};
#endif // Particle2MCChi2_H
