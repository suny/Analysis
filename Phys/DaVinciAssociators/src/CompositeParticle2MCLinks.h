/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CompositeParticle2MCLinks_H
#define CompositeParticle2MCLinks_H 1

// Include files
// from STL
#include <string>

// from LHCb
#include "Kernel/IParticlePropertySvc.h"

// local
#include "AsctAlgorithm.h"
#include "Kernel/Particle2MCLinker.h"

/** @class CompositeParticle2MCLinks CompositeParticle2MCLinks.h
 *
 *  @author Gerhard Raven
 *  @date   September 2002
 */
class CompositeParticle2MCLinks : public AsctAlgorithm {
public:
  /// Standard constructor
  CompositeParticle2MCLinks( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CompositeParticle2MCLinks(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

  bool isResonance( const LHCb::Particle* ) const;

private:
  // Properties
  bool        m_allowExtraMCPhotons;
  bool        m_inclusiveMode;
  bool        m_skipResonances;
  double      m_maxResonanceLifeTime;
  std::string m_asctMethod;
  bool        m_ignorePID;
  // Local variables
  LHCb::IParticlePropertySvc* m_ppSvc;
  Object2MCLinker<>*          m_p2MCLink;
  Object2MCLinker<>*          m_p2MCComp;
  int                         m_gamma;
  Particle2MCLinker::Linker*  m_linkerTable;
  int                         m_nrel;
  int                         m_nass;
  // Private methods
  bool associateTree( const LHCb::Particle* p, const LHCb::MCParticle* m );

  bool addDaughters( const LHCb::Particle* m, std::vector<const LHCb::Particle*>& daughters ) const;
  bool addMCDaughters( const LHCb::MCParticle* m, std::vector<const LHCb::MCParticle*>& daughters ) const;
};
#endif // CompositeParticle2MCLinks_H
