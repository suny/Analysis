/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef Particle2MCWithChi2_H
#define Particle2MCWithChi2_H 1

// Include files
// from STL
#include <string>

// histograms
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

// vectors and matrices
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/Vector4DTypes.h"

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// local
#include "AsctAlgorithm.h"
#include "Kernel/Particle2MCLinker.h"

#include "GaudiKernel/PhysicalConstants.h"
/** @class Particle2MCWithChi2 Particle2MCWithChi2.h
 *
 *
 *  @author Philippe Charpentier
 *  @date   11/04/2002
 */
class Particle2MCWithChi2 : public AsctAlgorithm {
public:
  /// Standard constructor
  Particle2MCWithChi2( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~Particle2MCWithChi2(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
  /// Fill vector of MCParticles parameters in "Track" state
  void get6Vector( const LHCb::MCParticle* mcPart, const double axz, Gaudi::Vector6& mcpVector );

private:
  /// Histograms
  bool          m_histos;            ///< Flag to fill histograms
  IHistogram1D* m_hisChi2;           ///< Chi2 histogram
  IHistogram2D* m_hisChi2vsDiffP;    ///< Chi2 vs Dp/p histo
  IHistogram2D* m_hisMinChi2vsDiffP; ///< Min Chi2 vs Dp/p histo

  // Chi2 cut
  double m_chi2SpeedUpCut; ///< Chi2 cut of parent Particle2MCWithChi2 algorithm, if present
};
#endif // Particle2MCWithChi2_H
