/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ASCTALGORITHM_H
#define ASCTALGORITHM_H 1

// Include files
// from STL
#include "boost/lexical_cast.hpp"
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/KeyedObject.h"

/** @class AsctAlgorithm AsctAlgorithm.h
 *
 *  Base class for algorithms creating Association tables
 *  @author Philippe Charpentier
 *  @date   2002-07-16
 */

class AsctAlgorithm : public GaudiAlgorithm {
public:
  /// Standard constructor
  AsctAlgorithm( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiAlgorithm( name, pSvcLocator ) {
    declareProperty( "InputData", m_inputData );
    declareProperty( "OutputTable", m_outputTable = "" );
  };

  virtual ~AsctAlgorithm(){}; ///< Destructor

  /// Location of table in TES
  std::string outputTable() { return m_outputTable; };

protected:
  std::vector<std::string> m_inputData;   ///< location of input data in the TES
  std::string              m_outputTable; ///< location of relations table
  inline std::string       objectName( const KeyedObject<int>* obj ) const {
    if ( !obj->hasKey() || NULL == obj->parent() || NULL == obj->parent()->registry() ) return "noKey";
    return obj->parent()->registry()->identifier() + "/" + boost::lexical_cast<std::string>( obj->key() );
  }
};

//=============================================================================
#endif // ASCTALGORITHM_H
