/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL:
// ============================================================================
#include <limits>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/GaudiException.h"
// ============================================================================
// Local
// ============================================================================
#include "Analysis/Iterator.h"
#include "Analysis/Models2D.h"
// ============================================================================
// ROOT
// ============================================================================
#include "RooArgSet.h"
#include "RooRealVar.h"
// ============================================================================
/** @file
 *  Implementation file for namespace Analysis::Models
 *  @author Vanya BELYAEV  Ivan.Belyaev@cern.ch
 *  @date   2011-11-30
 */
// ============================================================================

// ============================================================================
// generic polinomial
// ============================================================================
Analysis::Models::Poly2DPositive::Poly2DPositive( const char* name, const char* title, RooRealVar& x, RooRealVar& y,
                                                  const unsigned short nX, const unsigned short nY, RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_positive( nX, nY, x.getMin(), x.getMax(), y.getMin(), y.getMax() ) {
  //
  RooAbsArg*         coef = 0;
  unsigned int       num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_positive.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_positive.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::Poly2DPositive", StatusCode::FAILURE );
  }

  //
  setPars();
}
// ============================================================================
// copy constructor
// ============================================================================
Analysis::Models::Poly2DPositive::Poly2DPositive( const Analysis::Models::Poly2DPositive& right, const char* name )
    : RooAbsPdf( right, name )
    //
    , m_x( "x", this, right.m_x )
    , m_y( "y", this, right.m_y )
    , m_phis( "phis", this, right.m_phis )
    //
    , m_positive( right.m_positive ) {
  setPars();
}
// ============================================================================
// destructor
// ============================================================================
Analysis::Models::Poly2DPositive::~Poly2DPositive() {}
// ============================================================================
// clone
// ============================================================================
Analysis::Models::Poly2DPositive* Analysis::Models::Poly2DPositive::clone( const char* name ) const {
  return new Analysis::Models::Poly2DPositive( *this, name );
}
// ============================================================================
void Analysis::Models::Poly2DPositive::setPars() const {
  //
  RooAbsArg*       phi  = 0;
  const RooArgSet* nset = m_phis.nset();
  //
  unsigned short     k = 0;
  Analysis::Iterator it( m_phis );
  while ( ( phi = (RooAbsArg*)it.next() ) ) {
    const RooAbsReal* r = dynamic_cast<RooAbsReal*>( phi );
    if ( 0 == r ) { continue; }
    //
    const double phiv = r->getVal( nset );
    //
    m_positive.setPar( k, phiv );
    //
    ++k;
  }
  //
}
// ============================================================================
// the actual evaluation of function
// ============================================================================
Double_t Analysis::Models::Poly2DPositive::evaluate() const {
  //
  setPars();
  //
  return m_positive( m_x, m_y );
}
// ============================================================================
Int_t Analysis::Models::Poly2DPositive::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars,
                                                               const char* /* rangename */ ) const {
  if ( matchArgs( allVars, analVars, m_x, m_y ) ) {
    return 1;
  } else if ( matchArgs( allVars, analVars, m_x ) ) {
    return 2;
  } else if ( matchArgs( allVars, analVars, m_y ) ) {
    return 3;
  }
  //
  return 0;
}
//_____________________________________________________________________________
Double_t Analysis::Models::Poly2DPositive::analyticalIntegral( Int_t code, const char* rangeName ) const {
  assert( 1 == code || 2 == code || 3 == code );
  //
  setPars();
  //
  return 1 == code
             ? m_positive.integral( m_x.min( rangeName ), m_x.max( rangeName ), m_y.min( rangeName ),
                                    m_y.max( rangeName ) )
             : 2 == code ? m_positive.integrateX( m_y, m_x.min( rangeName ), m_x.max( rangeName ) )
                         : 3 == code ? m_positive.integrateY( m_x, m_y.min( rangeName ), m_y.max( rangeName ) ) : 0.0;
}
// ============================================================================

// ============================================================================
// generic polinomial
// ============================================================================
Analysis::Models::Poly2DSymPositive::Poly2DSymPositive( const char* name, const char* title, RooRealVar& x,
                                                        RooRealVar& y, const unsigned short n, RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_positive( n, std::min( x.getMin(), y.getMin() ), std::max( x.getMax(), y.getMax() ) ) {
  //
  RooAbsArg*         coef = 0;
  unsigned           num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_positive.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_positive.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::Poly2DSymPositive", StatusCode::FAILURE );
  }

  //
  setPars();
}

// ============================================================================
// copy constructor
// ============================================================================
Analysis::Models::Poly2DSymPositive::Poly2DSymPositive( const Analysis::Models::Poly2DSymPositive& right,
                                                        const char*                                name )
    : RooAbsPdf( right, name )
    //
    , m_x( "x", this, right.m_x )
    , m_y( "y", this, right.m_y )
    , m_phis( "phis", this, right.m_phis )
    //
    , m_positive( right.m_positive ) {
  setPars();
}
// ============================================================================
// destructor
// ============================================================================
Analysis::Models::Poly2DSymPositive::~Poly2DSymPositive() {}
// ============================================================================
// clone
// ============================================================================
Analysis::Models::Poly2DSymPositive* Analysis::Models::Poly2DSymPositive::clone( const char* name ) const {
  return new Analysis::Models::Poly2DSymPositive( *this, name );
}
// ============================================================================
void Analysis::Models::Poly2DSymPositive::setPars() const {
  //
  RooAbsArg*       phi  = 0;
  const RooArgSet* nset = m_phis.nset();
  //
  unsigned short     k = 0;
  Analysis::Iterator it( m_phis );
  while ( ( phi = (RooAbsArg*)it.next() ) ) {
    const RooAbsReal* r = dynamic_cast<RooAbsReal*>( phi );
    if ( 0 == r ) { continue; }
    //
    const double phiv = r->getVal( nset );
    //
    m_positive.setPar( k, phiv );
    //
    ++k;
  }
  //
}
// ============================================================================
// the actual evaluation of function
// ============================================================================
Double_t Analysis::Models::Poly2DSymPositive::evaluate() const {
  //
  setPars();
  //
  return m_positive( m_x, m_y );
}
// ============================================================================
Int_t Analysis::Models::Poly2DSymPositive::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars,
                                                                  const char* /* rangename */ ) const {
  if ( matchArgs( allVars, analVars, m_x, m_y ) ) {
    return 1;
  } else if ( matchArgs( allVars, analVars, m_x ) ) {
    return 2;
  } else if ( matchArgs( allVars, analVars, m_y ) ) {
    return 3;
  }
  //
  return 0;
}
// ============================================================================
Double_t Analysis::Models::Poly2DSymPositive::analyticalIntegral( Int_t code, const char* rangeName ) const {
  assert( 1 == code || 2 == code || 3 == code );
  //
  setPars();
  //
  return 1 == code
             ? m_positive.integral( m_x.min( rangeName ), m_x.max( rangeName ), m_y.min( rangeName ),
                                    m_y.max( rangeName ) )
             : 2 == code ? m_positive.integrateX( m_y, m_x.min( rangeName ), m_x.max( rangeName ) )
                         : 3 == code ? m_positive.integrateY( m_x, m_y.min( rangeName ), m_y.max( rangeName ) ) : 0.0;
}

// ============================================================================
//  PS(x)*PS(y)*Polynom
// ============================================================================
Analysis::Models::PS2DPol::PS2DPol( const char* name, const char* title, RooRealVar& x, RooRealVar& y,
                                    const Gaudi::Math::PhaseSpaceNL& psx, const Gaudi::Math::PhaseSpaceNL& psy,
                                    const unsigned short nX, const unsigned short nY, RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_function( psx, psy, nX, nY, x.getMin(), x.getMax(), y.getMin(), y.getMax() ) {
  //
  RooAbsArg*         coef = 0;
  unsigned           num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_function.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_function.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::PS22DPol", StatusCode::FAILURE );
  }

  //
}
// ============================================================================
// generic polinomial
// ============================================================================
Analysis::Models::PS2DPol::PS2DPol( const char* name, const char* title, RooRealVar& x, RooRealVar& y,
                                    const Gaudi::Math::PS2DPol& ps, RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_function( ps ) {
  //
  RooAbsArg*         coef = 0;
  unsigned           num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_function.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_function.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::PS22DPol", StatusCode::FAILURE );
  }

  //
  setPars();
}
// ============================================================================
// copy constructor
// ============================================================================
Analysis::Models::PS2DPol::PS2DPol( const Analysis::Models::PS2DPol& right, const char* name )
    : RooAbsPdf( right, name )
    //
    , m_x( "x", this, right.m_x )
    , m_y( "y", this, right.m_y )
    , m_phis( "phis", this, right.m_phis )
    //
    , m_function( right.m_function ) {
  setPars();
}
// ============================================================================
// destructor
// ============================================================================
Analysis::Models::PS2DPol::~PS2DPol() {}
// ============================================================================
// clone
// ============================================================================
Analysis::Models::PS2DPol* Analysis::Models::PS2DPol::clone( const char* name ) const {
  return new Analysis::Models::PS2DPol( *this, name );
}
// ============================================================================
void Analysis::Models::PS2DPol::setPars() const {
  //
  RooAbsArg*       phi  = 0;
  const RooArgSet* nset = m_phis.nset();
  //
  unsigned short     k = 0;
  Analysis::Iterator it( m_phis );
  while ( ( phi = (RooAbsArg*)it.next() ) ) {
    const RooAbsReal* r = dynamic_cast<RooAbsReal*>( phi );
    if ( 0 == r ) { continue; }
    //
    const double phiv = r->getVal( nset );
    //
    m_function.setPar( k, phiv );
    //
    ++k;
  }
  //
}

// ============================================================================
// the actual evaluation of function
// ============================================================================
Double_t Analysis::Models::PS2DPol::evaluate() const {
  //
  setPars();
  //
  return m_function( m_x, m_y );
}
// ============================================================================
Int_t Analysis::Models::PS2DPol::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars,
                                                        const char* /* rangename */ ) const {
  if ( matchArgs( allVars, analVars, m_x, m_y ) ) {
    return 1;
  } else if ( matchArgs( allVars, analVars, m_x ) ) {
    return 2;
  } else if ( matchArgs( allVars, analVars, m_y ) ) {
    return 3;
  }
  //
  return 0;
}
// ============================================================================
Double_t Analysis::Models::PS2DPol::analyticalIntegral( Int_t code, const char* rangeName ) const {
  assert( 1 == code || 2 == code || 3 == code );
  //
  setPars();
  //
  return 1 == code
             ? m_function.integral( m_x.min( rangeName ), m_x.max( rangeName ), m_y.min( rangeName ),
                                    m_y.max( rangeName ) )
             : 2 == code ? m_function.integrateX( m_y, m_x.min( rangeName ), m_x.max( rangeName ) )
                         : 3 == code ? m_function.integrateY( m_x, m_y.min( rangeName ), m_y.max( rangeName ) ) : 0.0;
}
// ============================================================================
//  PS(x)*PS(y)*SymPolynom
// ============================================================================
Analysis::Models::PS2DPolSym::PS2DPolSym( const char* name, const char* title, RooRealVar& x, RooRealVar& y,
                                          const Gaudi::Math::PhaseSpaceNL& ps, const unsigned short n,
                                          RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_function( ps, n, x.getMin(), x.getMax() ) {
  //
  RooAbsArg*         coef = 0;
  unsigned           num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_function.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_function.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::PS22DPol", StatusCode::FAILURE );
  }

  //
}
// ============================================================================
// generic polinomial
// ============================================================================
Analysis::Models::PS2DPolSym::PS2DPolSym( const char* name, const char* title, RooRealVar& x, RooRealVar& y,
                                          const Gaudi::Math::PS2DPolSym& ps, RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_function( ps ) {
  //
  RooAbsArg*         coef = 0;
  unsigned           num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_function.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_function.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::PS22DPol", StatusCode::FAILURE );
  }

  //
  setPars();
}
// ============================================================================
// copy constructor
// ============================================================================
Analysis::Models::PS2DPolSym::PS2DPolSym( const Analysis::Models::PS2DPolSym& right, const char* name )
    : RooAbsPdf( right, name )
    //
    , m_x( "x", this, right.m_x )
    , m_y( "y", this, right.m_y )
    , m_phis( "phis", this, right.m_phis )
    //
    , m_function( right.m_function ) {
  setPars();
}
// ============================================================================
// destructor
// ============================================================================
Analysis::Models::PS2DPolSym::~PS2DPolSym() {}
// ============================================================================
// clone
// ============================================================================
Analysis::Models::PS2DPolSym* Analysis::Models::PS2DPolSym::clone( const char* name ) const {
  return new Analysis::Models::PS2DPolSym( *this, name );
}
// ============================================================================
void Analysis::Models::PS2DPolSym::setPars() const {
  RooAbsArg*       phi  = 0;
  const RooArgSet* nset = m_phis.nset();
  //
  unsigned short     k = 0;
  Analysis::Iterator it( m_phis );
  while ( ( phi = (RooAbsArg*)it.next() ) ) {
    const RooAbsReal* r = dynamic_cast<RooAbsReal*>( phi );
    if ( 0 == r ) { continue; }
    //
    const double phiv = r->getVal( nset );
    //
    m_function.setPar( k, phiv );
    //
    ++k;
  }
}
// ============================================================================
// the actual evaluation of function
// ============================================================================
Double_t Analysis::Models::PS2DPolSym::evaluate() const {
  //
  setPars();
  //
  return m_function( m_x, m_y );
}
// ============================================================================
Int_t Analysis::Models::PS2DPolSym::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars,
                                                           const char* /* rangename */ ) const {
  if ( matchArgs( allVars, analVars, m_x, m_y ) ) {
    return 1;
  } else if ( matchArgs( allVars, analVars, m_x ) ) {
    return 2;
  } else if ( matchArgs( allVars, analVars, m_y ) ) {
    return 3;
  }
  //
  return 0;
}
// ============================================================================
Double_t Analysis::Models::PS2DPolSym::analyticalIntegral( Int_t code, const char* rangeName ) const {
  assert( 1 == code || 2 == code || 3 == code );
  //
  setPars();
  //
  return 1 == code
             ? m_function.integral( m_x.min( rangeName ), m_x.max( rangeName ), m_y.min( rangeName ),
                                    m_y.max( rangeName ) )
             : 2 == code ? m_function.integrateX( m_y, m_x.min( rangeName ), m_x.max( rangeName ) )
                         : 3 == code ? m_function.integrateY( m_x, m_y.min( rangeName ), m_y.max( rangeName ) ) : 0.0;
}

// ============================================================================
//  exp(x)*PS(y)*Polynom
// ============================================================================
Analysis::Models::ExpoPS2DPol::ExpoPS2DPol( const char* name, const char* title, RooRealVar& x, RooRealVar& y,
                                            RooAbsReal& tau, const Gaudi::Math::PhaseSpaceNL& psy,
                                            const unsigned short nX, const unsigned short nY, RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_tau( "tau", "Exponential slope", this, tau )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_function( psy, x.getMin(), x.getMax(), nX, nY, y.getMin(), y.getMax() ) {
  //
  RooAbsArg*         coef = 0;
  unsigned           num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_function.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_function.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::ExpoPS22DPol", StatusCode::FAILURE );
  }
}
// ============================================================================
// generic polinomial
// ============================================================================
Analysis::Models::ExpoPS2DPol::ExpoPS2DPol( const char* name, const char* title, RooRealVar& x, RooRealVar& y,
                                            RooAbsReal& tau, const Gaudi::Math::ExpoPS2DPol& ps, RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_tau( "tau", "Exponential slope", this, tau )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_function( ps ) {
  //
  RooAbsArg*         coef = 0;
  unsigned           num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_function.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_function.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::ExpoPS22DPol", StatusCode::FAILURE );
  }

  //
  setPars();
}
// ============================================================================
// copy constructor
// ============================================================================
Analysis::Models::ExpoPS2DPol::ExpoPS2DPol( const Analysis::Models::ExpoPS2DPol& right, const char* name )
    : RooAbsPdf( right, name )
    //
    , m_x( "x", this, right.m_x )
    , m_y( "y", this, right.m_y )
    , m_tau( "tau", this, right.m_tau )
    , m_phis( "phis", this, right.m_phis )
    //
    , m_function( right.m_function ) {
  setPars();
}
// ============================================================================
// destructor
// ============================================================================
Analysis::Models::ExpoPS2DPol::~ExpoPS2DPol() {}
// ============================================================================
// clone
// ============================================================================
Analysis::Models::ExpoPS2DPol* Analysis::Models::ExpoPS2DPol::clone( const char* name ) const {
  return new Analysis::Models::ExpoPS2DPol( *this, name );
}
// ============================================================================
void Analysis::Models::ExpoPS2DPol::setPars() const {
  //
  RooAbsArg*       phi  = 0;
  const RooArgSet* nset = m_phis.nset();
  //
  unsigned short     k = 0;
  Analysis::Iterator it( m_phis );
  while ( ( phi = (RooAbsArg*)it.next() ) ) {
    const RooAbsReal* r = dynamic_cast<RooAbsReal*>( phi );
    if ( 0 == r ) { continue; }
    //
    const double phiv = r->getVal( nset );
    //
    m_function.setPar( k, phiv );
    //
    ++k;
  }
  //
  m_function.setTau( m_tau );
}
// ============================================================================
// the actual evaluation of function
// ============================================================================
Double_t Analysis::Models::ExpoPS2DPol::evaluate() const {
  //
  setPars();
  //
  return m_function( m_x, m_y );
}
// ============================================================================
Int_t Analysis::Models::ExpoPS2DPol::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars,
                                                            const char* /* rangename */ ) const {
  if ( matchArgs( allVars, analVars, m_x, m_y ) ) {
    return 1;
  } else if ( matchArgs( allVars, analVars, m_x ) ) {
    return 2;
  } else if ( matchArgs( allVars, analVars, m_y ) ) {
    return 3;
  }
  //
  return 0;
}
// ============================================================================
Double_t Analysis::Models::ExpoPS2DPol::analyticalIntegral( Int_t code, const char* rangeName ) const {
  assert( 1 == code || 2 == code || 3 == code );
  //
  setPars();
  //
  return 1 == code
             ? m_function.integral( m_x.min( rangeName ), m_x.max( rangeName ), m_y.min( rangeName ),
                                    m_y.max( rangeName ) )
             : 2 == code ? m_function.integrateX( m_y, m_x.min( rangeName ), m_x.max( rangeName ) )
                         : 3 == code ? m_function.integrateY( m_x, m_y.min( rangeName ), m_y.max( rangeName ) ) : 0.0;
}

// ============================================================================
//  exp(x)*exp(y)*Polynom
// ============================================================================
Analysis::Models::Expo2DPol::Expo2DPol( const char* name, const char* title, RooRealVar& x, RooRealVar& y,
                                        RooAbsReal& taux, RooAbsReal& tauy, const unsigned short nX,
                                        const unsigned short nY, RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_taux( "taux", "Exponential slope", this, taux )
    , m_tauy( "tauy", "Exponential slope", this, tauy )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_function( x.getMin(), x.getMax(), y.getMin(), y.getMax(), nX, nY ) {
  //
  RooAbsArg*         coef = 0;
  unsigned           num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_function.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_function.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::ExpoPS22DPol", StatusCode::FAILURE );
  }

  //
  m_function.setTauX( m_taux );
  m_function.setTauY( m_tauy );
  //
  setPars();
}
// ============================================================================
// copy constructor
// ============================================================================
Analysis::Models::Expo2DPol::Expo2DPol( const Analysis::Models::Expo2DPol& right, const char* name )
    : RooAbsPdf( right, name )
    //
    , m_x( "x", this, right.m_x )
    , m_y( "y", this, right.m_y )
    , m_taux( "taux", this, right.m_taux )
    , m_tauy( "tauy", this, right.m_tauy )
    , m_phis( "phis", this, right.m_phis )
    //
    , m_function( right.m_function ) {
  setPars();
}
// ============================================================================
// destructor
// ============================================================================
Analysis::Models::Expo2DPol::~Expo2DPol() {}
// ============================================================================
// clone
// ============================================================================
Analysis::Models::Expo2DPol* Analysis::Models::Expo2DPol::clone( const char* name ) const {
  return new Analysis::Models::Expo2DPol( *this, name );
}
// ============================================================================
void Analysis::Models::Expo2DPol::setPars() const {
  //
  RooAbsArg*       phi  = 0;
  const RooArgSet* nset = m_phis.nset();
  //
  unsigned short     k = 0;
  Analysis::Iterator it( m_phis );
  while ( ( phi = (RooAbsArg*)it.next() ) ) {
    const RooAbsReal* r = dynamic_cast<RooAbsReal*>( phi );
    if ( 0 == r ) { continue; }
    //
    const double phiv = r->getVal( nset );
    //
    m_function.setPar( k, phiv );
    //
    ++k;
  }
  //
  m_function.setTauX( m_taux );
  m_function.setTauY( m_tauy );
  //
}
// ============================================================================
// the actual evaluation of function
// ============================================================================
Double_t Analysis::Models::Expo2DPol::evaluate() const {
  //
  setPars();
  //
  return m_function( m_x, m_y );
}
// ============================================================================
Int_t Analysis::Models::Expo2DPol::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars,
                                                          const char* /* rangename */ ) const {
  if ( matchArgs( allVars, analVars, m_x, m_y ) ) {
    return 1;
  } else if ( matchArgs( allVars, analVars, m_x ) ) {
    return 2;
  } else if ( matchArgs( allVars, analVars, m_y ) ) {
    return 3;
  }
  //
  return 0;
}
// ============================================================================
Double_t Analysis::Models::Expo2DPol::analyticalIntegral( Int_t code, const char* rangeName ) const {
  assert( 1 == code || 2 == code || 3 == code );
  //
  setPars();
  //
  return 1 == code
             ? m_function.integral( m_x.min( rangeName ), m_x.max( rangeName ), m_y.min( rangeName ),
                                    m_y.max( rangeName ) )
             : 2 == code ? m_function.integrateX( m_y, m_x.min( rangeName ), m_x.max( rangeName ) )
                         : 3 == code ? m_function.integrateY( m_x, m_y.min( rangeName ), m_y.max( rangeName ) ) : 0.0;
}

// ============================================================================
//  exp(x)*exp(y)*SymPolynom
// ============================================================================
Analysis::Models::Expo2DPolSym::Expo2DPolSym( const char* name, const char* title, RooRealVar& x, RooRealVar& y,
                                              RooAbsReal& tau, const unsigned short n, RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_tau( "tau", "Exponential slope", this, tau )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_function( x.getMin(), x.getMax(), n ) {
  //
  RooAbsArg*         coef = 0;
  unsigned           num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_function.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_function.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::ExpoPS22DPol", StatusCode::FAILURE );
  }

  //
  setPars();
}
// ============================================================================
// copy constructor
// ============================================================================
Analysis::Models::Expo2DPolSym::Expo2DPolSym( const Analysis::Models::Expo2DPolSym& right, const char* name )
    : RooAbsPdf( right, name )
    //
    , m_x( "x", this, right.m_x )
    , m_y( "y", this, right.m_y )
    , m_tau( "tau", this, right.m_tau )
    , m_phis( "phis", this, right.m_phis )
    //
    , m_function( right.m_function ) {
  setPars();
}
// ============================================================================
// destructor
// ============================================================================
Analysis::Models::Expo2DPolSym::~Expo2DPolSym() {}
// ============================================================================
// clone
// ============================================================================
Analysis::Models::Expo2DPolSym* Analysis::Models::Expo2DPolSym::clone( const char* name ) const {
  return new Analysis::Models::Expo2DPolSym( *this, name );
}
// ============================================================================
void Analysis::Models::Expo2DPolSym::setPars() const {
  RooAbsArg*       phi  = 0;
  const RooArgSet* nset = m_phis.nset();
  //
  unsigned short     k = 0;
  Analysis::Iterator it( m_phis );
  while ( ( phi = (RooAbsArg*)it.next() ) ) {
    const RooAbsReal* r = dynamic_cast<RooAbsReal*>( phi );
    if ( 0 == r ) { continue; }
    //
    const double phiv = r->getVal( nset );
    //
    m_function.setPar( k, phiv );
    //
    ++k;
  }
  //
  m_function.setTau( m_tau );
  //
}
// ============================================================================
// the actual evaluation of function
// ============================================================================
Double_t Analysis::Models::Expo2DPolSym::evaluate() const {
  //
  setPars();
  //
  return m_function( m_x, m_y );
}
// ============================================================================
Int_t Analysis::Models::Expo2DPolSym::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars,
                                                             const char* /* rangename */ ) const {
  if ( matchArgs( allVars, analVars, m_x, m_y ) ) {
    return 1;
  } else if ( matchArgs( allVars, analVars, m_x ) ) {
    return 2;
  } else if ( matchArgs( allVars, analVars, m_y ) ) {
    return 3;
  }
  //
  return 0;
}
// ============================================================================
Double_t Analysis::Models::Expo2DPolSym::analyticalIntegral( Int_t code, const char* rangeName ) const {
  assert( 1 == code || 2 == code || 3 == code );
  //
  setPars();
  //
  return 1 == code
             ? m_function.integral( m_x.min( rangeName ), m_x.max( rangeName ), m_y.min( rangeName ),
                                    m_y.max( rangeName ) )
             : 2 == code ? m_function.integrateX( m_y, m_x.min( rangeName ), m_x.max( rangeName ) )
                         : 3 == code ? m_function.integrateY( m_x, m_y.min( rangeName ), m_y.max( rangeName ) ) : 0.0;
}
// ============================================================================

// ============================================================================
// generic 2D-spline
// ============================================================================
Analysis::Models::Spline2D::Spline2D( const char* name, const char* title, RooRealVar& x, RooRealVar& y,
                                      const Gaudi::Math::Spline2D& spline, RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_spline( spline ) {
  //
  RooAbsArg*         coef = 0;
  unsigned int       num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_spline.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_spline.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::Spline2D", StatusCode::FAILURE );
  }

  //
  setPars();
}
// ============================================================================
// copy constructor
// ============================================================================
Analysis::Models::Spline2D::Spline2D( const Analysis::Models::Spline2D& right, const char* name )
    : RooAbsPdf( right, name )
    //
    , m_x( "x", this, right.m_x )
    , m_y( "y", this, right.m_y )
    , m_phis( "phis", this, right.m_phis )
    //
    , m_spline( right.m_spline ) {
  setPars();
}
// ============================================================================
// destructor
// ============================================================================
Analysis::Models::Spline2D::~Spline2D() {}
// ============================================================================
// clone
// ============================================================================
Analysis::Models::Spline2D* Analysis::Models::Spline2D::clone( const char* name ) const {
  return new Analysis::Models::Spline2D( *this, name );
}
// ============================================================================
void Analysis::Models::Spline2D::setPars() const {
  //
  RooAbsArg*       phi  = 0;
  const RooArgSet* nset = m_phis.nset();
  //
  unsigned short     k = 0;
  Analysis::Iterator it( m_phis );
  while ( ( phi = (RooAbsArg*)it.next() ) ) {
    const RooAbsReal* r = dynamic_cast<RooAbsReal*>( phi );
    if ( 0 == r ) { continue; }
    //
    const double phiv = r->getVal( nset );
    //
    m_spline.setPar( k, phiv );
    //
    ++k;
  }
  //
}
// ============================================================================
// the actual evaluation of function
// ============================================================================
Double_t Analysis::Models::Spline2D::evaluate() const {
  //
  setPars();
  //
  return m_spline( m_x, m_y );
}
// ============================================================================
Int_t Analysis::Models::Spline2D::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars,
                                                         const char* /* rangename */ ) const {
  if ( matchArgs( allVars, analVars, m_x, m_y ) ) {
    return 1;
  } else if ( matchArgs( allVars, analVars, m_x ) ) {
    return 2;
  } else if ( matchArgs( allVars, analVars, m_y ) ) {
    return 3;
  }
  //
  return 0;
}
//_____________________________________________________________________________
Double_t Analysis::Models::Spline2D::analyticalIntegral( Int_t code, const char* rangeName ) const {
  //
  assert( 1 == code || 2 == code || 3 == code );
  //
  setPars();
  //
  return 1 == code
             ? m_spline.integral( m_x.min( rangeName ), m_x.max( rangeName ), m_y.min( rangeName ),
                                  m_y.max( rangeName ) )
             : 2 == code ? m_spline.integrateX( m_y, m_x.min( rangeName ), m_x.max( rangeName ) )
                         : 3 == code ? m_spline.integrateY( m_x, m_y.min( rangeName ), m_y.max( rangeName ) ) : 0.0;
}

// ============================================================================
// generic 2D symmetric spline
// ============================================================================
Analysis::Models::Spline2DSym::Spline2DSym( const char* name, const char* title, RooRealVar& x, RooRealVar& y,
                                            const Gaudi::Math::Spline2DSym& spline, RooArgList& phis )
    : RooAbsPdf( name, title )
    , m_x( "x", "Observable-X", this, x )
    , m_y( "y", "Observable-Y", this, y )
    , m_phis( "phis", "Coefficients", this )
    //
    , m_spline( spline ) {
  //
  RooAbsArg*         coef = 0;
  unsigned int       num  = 0;
  Analysis::Iterator tmp( phis );
  while ( ( coef = (RooAbsArg*)tmp.next() ) && num < m_spline.npars() ) {
    RooAbsReal* r = dynamic_cast<RooAbsReal*>( coef );
    if ( 0 == r ) { continue; }
    m_phis.add( *coef );
    ++num;
  }
  //
  if ( num != m_spline.npars() ) {
    throw GaudiException( "Invalid size of parameters vector", "Analysis::Spline2DSym", StatusCode::FAILURE );
  }

  //
  setPars();
}
// ============================================================================
// copy constructor
// ============================================================================
Analysis::Models::Spline2DSym::Spline2DSym( const Analysis::Models::Spline2DSym& right, const char* name )
    : RooAbsPdf( right, name )
    //
    , m_x( "x", this, right.m_x )
    , m_y( "y", this, right.m_y )
    , m_phis( "phis", this, right.m_phis )
    //
    , m_spline( right.m_spline ) {
  setPars();
}
// ============================================================================
// destructor
// ============================================================================
Analysis::Models::Spline2DSym::~Spline2DSym() {}
// ============================================================================
// clone
// ============================================================================
Analysis::Models::Spline2DSym* Analysis::Models::Spline2DSym::clone( const char* name ) const {
  return new Analysis::Models::Spline2DSym( *this, name );
}
// ============================================================================
void Analysis::Models::Spline2DSym::setPars() const {
  //
  RooAbsArg*       phi  = 0;
  const RooArgSet* nset = m_phis.nset();
  //
  unsigned short     k = 0;
  Analysis::Iterator it( m_phis );
  while ( ( phi = (RooAbsArg*)it.next() ) ) {
    const RooAbsReal* r = dynamic_cast<RooAbsReal*>( phi );
    if ( 0 == r ) { continue; }
    //
    const double phiv = r->getVal( nset );
    //
    m_spline.setPar( k, phiv );
    //
    ++k;
  }
  //
}

// ============================================================================
// the actual evaluation of function
// ============================================================================
Double_t Analysis::Models::Spline2DSym::evaluate() const {
  //
  setPars();
  //
  return m_spline( m_x, m_y );
}
// ============================================================================
Int_t Analysis::Models::Spline2DSym::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars,
                                                            const char* /* rangename */ ) const {
  if ( matchArgs( allVars, analVars, m_x, m_y ) ) {
    return 1;
  } else if ( matchArgs( allVars, analVars, m_x ) ) {
    return 2;
  } else if ( matchArgs( allVars, analVars, m_y ) ) {
    return 3;
  }
  //
  return 0;
}
//_____________________________________________________________________________
Double_t Analysis::Models::Spline2DSym::analyticalIntegral( Int_t code, const char* rangeName ) const {
  //
  assert( 1 == code || 2 == code || 3 == code );
  //
  setPars();
  //
  return 1 == code
             ? m_spline.integral( m_x.min( rangeName ), m_x.max( rangeName ), m_y.min( rangeName ),
                                  m_y.max( rangeName ) )
             : 2 == code ? m_spline.integrateX( m_y, m_x.min( rangeName ), m_x.max( rangeName ) )
                         : 3 == code ? m_spline.integrateY( m_x, m_y.min( rangeName ), m_y.max( rangeName ) ) : 0.0;
}

// ============================================================================
#define CLASS_IMP ClassImp // make clang-format recognise the macro
CLASS_IMP( Analysis::Models::Poly2DPositive )
CLASS_IMP( Analysis::Models::Poly2DSymPositive )
CLASS_IMP( Analysis::Models::PS2DPol )
CLASS_IMP( Analysis::Models::PS2DPolSym )
CLASS_IMP( Analysis::Models::ExpoPS2DPol )
CLASS_IMP( Analysis::Models::Expo2DPol )
CLASS_IMP( Analysis::Models::Expo2DPolSym )
CLASS_IMP( Analysis::Models::Spline2D )
CLASS_IMP( Analysis::Models::Spline2DSym )
// ============================================================================
//                                                                      The END
// ============================================================================
