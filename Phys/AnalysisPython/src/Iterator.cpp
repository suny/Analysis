/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// ROOT&RooFit
// ============================================================================
#include "RooAbsCollection.h" // RooFit
#include "RooLinkedList.h"    // RooFit
#include "TCollection.h"      // ROOT
#include "TIterator.h"        // ROOT
// ============================================================================
// local
// ============================================================================
#include "Analysis/Iterator.h"
// ============================================================================
// standard constructor: create and keep the ietrator
// ============================================================================
Analysis::Iterator::Iterator( const RooAbsCollection& collection ) : m_iterator( collection.createIterator() ) {}
// ============================================================================
// standard constructor: create and keep the ietrator
// ============================================================================
Analysis::Iterator::Iterator( const TCollection& collection ) : m_iterator( collection.MakeIterator() ) {}
// ============================================================================
// standard constructor: create and keep the ietrator
// ============================================================================
Analysis::Iterator::Iterator( const RooLinkedList& collection ) : m_iterator( collection.MakeIterator() ) {}
// ============================================================================
// default constructor
// ============================================================================
Analysis::Iterator::Iterator() : m_iterator( 0 ) {}
// ============================================================================
// destructor
// ============================================================================
Analysis::Iterator::~Iterator() {
  if ( 0 != m_iterator ) {
    delete m_iterator;
    m_iterator = 0;
  }
}
// ============================================================================
// invoke TIterator::Next
// ============================================================================
TObject* Analysis::Iterator::next() const // invoke TIterator::Next
{
  return m_iterator ? m_iterator->Next() : 0;
}
// ============================================================================
// invoke TIterator::Reset
// ============================================================================
bool Analysis::Iterator::reset() const {
  if ( !m_iterator ) { return false; }
  m_iterator->Reset();
  return true;
}
// ============================================================================
// The END
// ============================================================================
