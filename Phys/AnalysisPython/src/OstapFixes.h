/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef OSTAPFIXES_H
#  define OSTAPFIXES_H 1
// ============================================================================
#  include "GaudiKernel/StatusCode.h"
// ============================================================================
namespace Ostap {
  // ==========================================================================
  using ::StatusCode;
  // ==========================================================================
  StatusCode throwException( const std::string& message, const std::string& tag,
                             const StatusCode& sc = StatusCode::FAILURE );
  // ==========================================================================
  inline bool Assert( const bool assertion, const std::string& message, const std::string& tag = "Ostap",
                      const StatusCode& sc = StatusCode::FAILURE ) {
    return assertion ? true : throwException( message, tag, sc ).isSuccess();
  }
  // ===========================================================================
  template <unsigned int N1, unsigned int N2>
  inline bool Assert( const bool        assertion, const char ( &message )[N1], const char ( &tag )[N2],
                      const StatusCode& sc = StatusCode::FAILURE ) {
    return assertion
               ? true
               : throwException( std::string( message, message + N1 ), std::string( tag, tag + N2 ), sc ).isSuccess();
  }
  // ===========================================================================
} // namespace Ostap
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // OSTAPFIXES_H
// ============================================================================
