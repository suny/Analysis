/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ===========================================================================
// Include files
// ===========================================================================
// GaudiKernel
// ===========================================================================
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"
// ============================================================================
// local
// ============================================================================
#include "OstapFixes.h"
// ============================================================================
/** @file
 *  Implementation file
 *  @date 2018-06-11
 *  @author Vanya Belyaev
 */
// ============================================================================
StatusCode Ostap::throwException( const std::string& message, const std::string& tag, const StatusCode& sc ) {
  throw GaudiException( message, tag, sc );
  return sc;
}
// ============================================================================
// The END
// ============================================================================
