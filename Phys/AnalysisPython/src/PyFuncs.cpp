/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD& STL
// ============================================================================
#include <climits>
#include <limits>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"
// ============================================================================
// Ostap and Python
// ============================================================================
#include "Analysis/PyFuncs.h"
// ============================================================================
/** @file
 *  Implementation file for classes from namespace Ostap::Functions
 *  @date   2018-03-31
 *  @author Vanya Belyaev
 */
// ============================================================================
namespace {
  // ===========================================================================
  static_assert( std::numeric_limits<float>::is_specialized, "std::numeric_limits<float> is not specialized" );
  // ==========================================================================
  constexpr double s_max = std::numeric_limits<float>::max();
  constexpr double s_min = -std::numeric_limits<float>::max();
  // ==========================================================================
  static_assert( s_max > 0, "std::numeric_limits<float>::max is too small" );
  static_assert( s_min < 0, "std::numeric_limits<float>::max is too small" );
  // ==========================================================================
  static char s_method[] = "evaluate";
  // =========================================================================
  double call_python( PyObject* self ) {
    // check arguments
    if ( !self ) { throw GaudiException( "Python exception: invalid ``self''", "call_python", StatusCode( 400 ) ); }
    // call Python
    PyObject* r = PyObject_CallMethod( self, s_method, nullptr );
    // error/exception ?
    if ( !r ) {
      PyErr_Print();
      throw GaudiException( "Python exception: invalid ``result''", "call_python", StatusCode( 500 ) );
    }
// float or integer
#if PY_MAJOR_VERSION >= 3
    if ( PyFloat_Check( r ) ) // floating value?
    {
      const double result = PyFloat_AS_DOUBLE( r );
      Py_DECREF( r );
      return result;                // RETURN
    } else if ( PyLong_Check( r ) ) // integer value ?
    {
      const double result = PyLong_AS_LONG( r );
      Py_DECREF( r );
      return result; //  RETURN
    }
#else
    if ( PyFloat_Check( r ) ) // floating value?
    {
      const double result = PyFloat_AS_DOUBLE( r );
      Py_DECREF( r );
      return result;               // RETURN
    } else if ( PyInt_Check( r ) ) // integer value ?
    {
      const double result = PyInt_AS_LONG( r );
      Py_DECREF( r );
      return result; //  RETURN
    }
#endif
    // ?
    const double result = PyFloat_AsDouble( r );
    if ( PyErr_Occurred() ) {
      PyErr_Print();
      Py_DECREF( r );
      throw GaudiException( "Python exception: invalid conversion", "call_python", StatusCode( 600 ) );
    }
    //
    Py_DECREF( r );
    return result; // RETURN
  }
  // ==========================================================================
} // namespace
// ============================================================================
/** constructor
 *  @param self python objects
 *  @param tree pointer to the tree
 */
// ============================================================================
Analysis::Functions::PyFuncTree::PyFuncTree( PyObject* self, const TTree* tree )
    : Analysis::IFuncTree(), m_tree( tree ), m_self( self ) {
  Py_INCREF( m_self );
}
// ============================================================================
// destructor
// ============================================================================
Analysis::Functions::PyFuncTree::~PyFuncTree() { Py_XDECREF( m_self ); }
// ============================================================================
// the basic
// ============================================================================
double Analysis::Functions::PyFuncTree::operator()( const TTree* t ) const {
  /// redefine the current  tree
  if ( nullptr != t ) { m_tree = t; }
  return call_python( m_self );
}
// ============================================================================
/** constructor
 *  @param self python objects
 *  @param tree pointer to the tree
 */
// ============================================================================
Analysis::Functions::PyFuncData::PyFuncData( PyObject* self, const RooAbsData* data )
    : Analysis::IFuncData(), m_data( data ), m_self( self ) {
  Py_INCREF( m_self );
}
// ============================================================================
// destructor
// ============================================================================
Analysis::Functions::PyFuncData::~PyFuncData() { Py_XDECREF( m_self ); }
// ============================================================================
// the basic
// ============================================================================
double Analysis::Functions::PyFuncData::operator()( const RooAbsData* d ) const {
  /// redefine the current  tree
  if ( nullptr != d ) { m_data = d; }
  return call_python( m_self );
}
// ======================================================================

// ============================================================================
// The END
// ============================================================================
