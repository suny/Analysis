/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/ToStream.h"
// ============================================================================
// AnalysisPython
// ============================================================================
#include "Analysis/Formula.h"
// ============================================================================
// ROOT
// ============================================================================
#include "TCut.h"
#include "TTree.h"
// ============================================================================
/** Implementation file for class Analysis::Formula
 *  @see Analysis::Formula
 *  @see TTreeFormula
 *  @date 2013-05-06
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 */
// ============================================================================
// constructor from name, expression and the tree
// ============================================================================
Analysis::Formula::Formula( const std::string& name, const std::string& expression, TTree* tree )
    : TTreeFormula( name.c_str(), expression.c_str(), tree ) {}
// ============================================================================
Analysis::Formula::Formula( const std::string& name, const TCut& expression, TTree* tree )
    : TTreeFormula( name.c_str(), expression, tree ) {}
// ============================================================================
// destructor
// ============================================================================
Analysis::Formula::~Formula() {
  TTree* tree = GetTree();
  if ( 0 != tree && this == tree->GetNotify() ) { tree->SetNotify( 0 ); }
}
// ============================================================================
// evaluate the formula (scalar)
// ============================================================================
double Analysis::Formula::evaluate() // evaluate the formula
{
  const Int_t d = GetNdata();
  if ( 1 != d ) {
    throw GaudiException( "evaluate: scalar call for GetNdata()=" + Gaudi::Utils::toString( d ) + " function \"" +
                              std::string( this->GetTitle() ) + "\"",
                          "Analysis::Formula", StatusCode::FAILURE );
  }
  return EvalInstance();
}
// ============================================================================
// evaluate the given instance of the formula
// ============================================================================
double Analysis::Formula::evaluate( const unsigned short i ) // evaluate the formula
{
  const Int_t d = GetNdata();
  return EvalInstance( i % d );
}
// ============================================================================
// evaluate all the instances of the formula
// ============================================================================
Int_t Analysis::Formula::evaluate( std::vector<double>& results ) {
  const Int_t d = GetNdata();
  results.resize( d );
  for ( Int_t i = 0; i < d; ++i ) { results[i] = EvalInstance( i ); }
  return d;
}
// ============================================================================
// The END
// ============================================================================
