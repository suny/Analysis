# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from AnalysisPython import Helpers


class AssocTreeDebugger:
    def __init__(self, assoc, particleName):
        self.assoc = assoc
        self.pName = particleName
        self.nTrees = {}

    def __call__(self, particle):
        trees = self.assoc.relatedMCPs(particle)
        if trees:
            size = trees.size()
            pid = Helpers.pid(particle)
            if pid in self.nTrees:
                if size in self.nTrees[pid]:
                    self.nTrees[pid][size] += 1
                else:
                    self.nTrees[pid][size] = 1
            else:
                self.nTrees[pid] = {size: 1}
            print("Found ", size, " trees for particle ", self.pName(particle))
            if size > 0:
                for tree in trees:
                    print("\t===========TREE========")
                    for p in tree:
                        print("\t", self.pName(p))
                    print("\t=======================")
        else:
            print("Found NO trees for particle ", self.pName(particle))
            if "None" in self.nTrees:
                self.nTrees["None"] += 1
            else:
                self.nTrees["None"] = 1


class WeightedDebugger:
    def __init__(self, assoc, particleName):
        self.assoc = assoc
        self.pName = particleName
        self.nTrees = {}

    def __call__(self, particle):
        mcps = self.assoc.relatedMCPs(particle)
        if mcps:
            size = mcps.size()
            pid = Helpers.pid(particle)
            if pid in self.nTrees:
                if size in self.nTrees[pid]:
                    self.nTrees[pid][size] += 1
                else:
                    self.nTrees[pid][size] = 1
            else:
                self.nTrees[pid] = {size: 1}
            if size > 1:
                print("Found ", size, " mcps for particle ",
                      self.pName(particle))
                print("\t===MCPs for Particle PID ", pid, " ===")
                for mcp in mcps:
                    print("\t", self.pName(mcp))
                print("\t==================================")
        else:
            if "None" in self.nTrees:
                self.nTrees["None"] += 1
            else:
                self.nTrees["None"] = 1
