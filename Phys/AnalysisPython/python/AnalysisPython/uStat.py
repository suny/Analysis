#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# ============================================================================
# ============================================================================
# @file AnalysisPython/uStat.py
#
# Helper module to get ``U-statistics'' useful for ``Goodnes-Of-Fit'' tests
#
# This is a simple translation of
#   the original C++ lines written by Greig Cowan into python
#
# Usage is fairly trivial:
#
#  @code
#
#   >>> pdf  = ...               ## pdf
#   >>> data = ...               ## dataset
#   >>> pdf.fitTo( data , ... )  ## fit it!
#
#   >>> import AnalysisPython.uStat as uStat
#
#   >>> r,histo = uStat.uPlot ( pdf , data )
#   >>> print r                  ## print fit results
#   >>> histo.Draw()             ## plot the results
#
#  @endcode
#
# @author Vanya Belyaev Ivan.Belyaev@cern.ch
# @date 2011-09-21
#
#
# ============================================================================
"""

Helper module to get ``U-statistics'' useful for ``Goodness-Of-Fit'' tests

This is a simple translation of
  the original C++ lines written by Greig Cowan into Python

Usage is fairly trivial:

   >>> pdf  = ...               ## pdf
   >>> data = ...               ## dataset
   >>> pdf.fitTo( data , ... )  ## fit it!

   >>> import AnalysisPython.uStat as uStat

   >>> r,histo = uStat.uPlot ( pdf , data )
   >>> print r                  ## print fit results
   >>> histo.Draw()             ## plot the results

"""
# ============================================================================
from __future__ import print_function
__author__ = "Vanya BELYAEV Ivan.Belyaev@cern.ch"
__date__ = "2010-09-21"
# ============================================================================
__all__ = (
    "uPlot",  ## make  plot of U-statistics
    "uDist",  ## calculate  U-statistics
    "uCalc",  ## calclulate the distance between two data points
)
# ============================================================================
import ROOT
# ============================================================================
import warnings
warnings.warn(
    """AnalysisPython:
    Use 'PyPAW.uStat' module instead of 'AnalysisPython.uStat'""",
    DeprecationWarning,
    stacklevel=3)
# =============================================================================
## the actual import
from PyPAW.uStat import *

# ===========================================================================

if '__main__' == __name__:

    print(80 * '*')
    print(__doc__)
    print(' Author  : ', __author__)
    print(' Date    : ', __date__)
    print(' Symbols : ', __all__)
    print(80 * '*')

# ===========================================================================
# The END
# ===========================================================================
