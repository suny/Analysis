#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# =============================================================================
##  This module contains set of simple and useful utilities for booking and
#   manipulations with Gaudi-AIDA histograms, inspired by Thomas' request
#
#   @author Vanya BELYAEV ibelyaev@physics.syr.edu
#   @date 2007-08-03
#
# =============================================================================
import warnings
warnings.warn(
    """
    AnalysisPython:
    Use 'GaudiPython.HistoUtils' module instead of 'HistoUtils'
    """,
    DeprecationWarning,
    stacklevel=3)
# =============================================================================
from GaudiPython.HistoUtils import *
# =============================================================================
# The END
# =============================================================================
