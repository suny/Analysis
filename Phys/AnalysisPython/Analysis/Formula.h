/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef ANALYSIS_FORMULA_H
#  define ANALYSIS_FORMULA_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#  include "GaudiKernel/Kernel.h"
// ============================================================================
// ROOT
// ============================================================================
#  include "TTreeFormula.h"
// ============================================================================
class TCut; // ROOT
// ============================================================================
namespace Analysis {
  // ==========================================================================
  /** @class Formula Analysis/Formula.h
   *
   *  Simple extention of class TTreeFormula
   *  for easier usage in python
   *
   *  @see TTreeFormula
   *  @author Vanya Belyaev
   *  @date   2013-05-06
   */
  class GAUDI_API Formula : public TTreeFormula {
    // ========================================================================
  public:
    // ========================================================================
    ClassDef( Analysis::Formula, 1 );
    // ========================================================================
  public:
    // ========================================================================
    /// constructor from name, expression and the tree
    Formula( const std::string& name, const std::string& expression, TTree* tree );
    /// constructor from name, expression and the tree
    Formula( const std::string& name, const TCut& expression, TTree* tree );
    /// virtual destructor
    virtual ~Formula();
    // ========================================================================
  public:
    // ========================================================================
    /// evaluate the scalar formula
    double evaluate(); // evaluate the formula
    /// evaluate the given instance of the formula
    double evaluate( unsigned short i ); // evaluate the formula
    ///  evaluate all instances
    Int_t evaluate( std::vector<double>& result );
    // is formula OK?
    bool ok() const { return this->GetNdim(); } // is formula OK ?
    // ========================================================================
  };
  // ==========================================================================
} //                                                  End of namespace Analysis
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // ANALYSIS_CUTFORMULA_H
// ============================================================================
