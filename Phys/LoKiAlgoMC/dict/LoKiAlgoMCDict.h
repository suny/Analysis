/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_LOKIALGOMCDICT_H
#  define LOKI_LOKIALGOMCDICT_H 1

// redefined anyway in features.h by _GNU_SOURCE
#  undef _XOPEN_SOURCE
#  undef _POSIX_C_SOURCE

// ============================================================================
// Python must always be the first.
#  ifndef __APPLE__
#    include "Python.h"
#  endif // not __APPLE__

// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/AlgoDecorator.h"
#  include "LoKi/LoKiAlgoMC.h"
#  include "LoKi/MCMatchAlgoDicts.h"
#  include "LoKi/MCTupleDicts.h"
// ============================================================================
namespace {
  struct __Instantiations {
    LoKi::Interface<LoKi::AlgoMC>  m_a1;
    LoKi::Dicts::Alg<LoKi::AlgoMC> m_a2;
    //
    __Instantiations();
  };
} // namespace
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_LOKIALGOMCDICT_H
// ============================================================================
