/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_MCMATCHALGODICTS_H
#  define LOKI_MCMATCHALGODICTS_H 1
// ============================================================================
// Include files
// ============================================================================
// Event
// ============================================================================
#  include "Event/MCParticle.h"
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/Keeper.h"
#  include "LoKi/Loop.h"
#  include "LoKi/MCMatch.h"
#  include "LoKi/MCTypes.h"
#  include "LoKi/UniqueKeeper.h"
// ============================================================================
namespace LoKi {
  namespace Dicts {
    namespace MCMatchDicts {
      // ======================================================================
      /// do these MC particles make contribution to this RC particle?
      bool match( const LoKi::MCMatch& m, const LoKi::Loop& l, const LHCb::MCParticle* mc );
      // ======================================================================
      /// do these MC particles make contribution to this RC particle?
      bool match( const LoKi::MCMatch& m, const LoKi::Loop& l, const LoKi::Types::MCRange& mc );
      // ======================================================================
      /// do these MC particles make contribution to this RC particle?
      bool match( const LoKi::MCMatch& m, const LoKi::Loop& l, const LHCb::MCParticle::Vector& mc );
      // ======================================================================
      /// do these MC particles make contribution to this RC particle?
      bool match( const LoKi::MCMatch& m, const LoKi::Loop& l, const LHCb::MCParticle::ConstVector& mc );
      // ======================================================================
      /// do these MC particles make contribution to this RC particle?
      bool match( const LoKi::MCMatch& m, const LoKi::Loop& l, const LoKi::Keeper<LHCb::MCParticle>& mc );
      // ======================================================================
      /// do these MC particles make contribution to this RC particle?
      bool match( const LoKi::MCMatch& m, const LoKi::Loop& l, const LoKi::UniqueKeeper<LHCb::MCParticle>& mc );
      // ======================================================================
    } // namespace MCMatchDicts
  }   // namespace Dicts
} // end of namespace LoKi
// =============================================================================
// The END
// =============================================================================
#endif // LOKI_MCMATCHALGODICTS_H
// =============================================================================
