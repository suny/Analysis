#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file scaling_2016.py
#
# Momentum scaling for 2016
#
# @author Matt  NEEDHAM
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date   2016-01-11
# ============================================================================
"""Momentum scaling fro 2016

Input data are specified close to the start of the script

- input ROOT file with historams
the file should have two 2D-histograms, named as 'idp-plus' and 'idp-minus'

- global delta
- list of run-dependent offsets as list of triplets:  [ (start,stop,offset) ],
where (start,stop) represent the run range (both edges are inclusive)

As the output xml-file MomentumScale.xml is generated in cwd

"""
# ============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2016-01-11"
# ============================================================================
import ROOT, Ostap.PyRoUts
# ============================================================================
from DaVinciTrackScaling.momentum_scale import buildXML, logger
# ============================================================================
logger.info(100 * '*')
logger.info(__doc__)
logger.info(100 * '*')
# ============================================================================

# ============================================================================
## the histograms
with ROOT.TFile('../../data/BFitResults-fine-2016-new.root',
                'READ') as the_file:
    idp_plus = the_file.Get('idp-plus').clone()
    idp_minus = the_file.Get('idp-minus').clone()
    logger.info('HISTOS are read from %s' % the_file)

# =============================================================================
## global delta
##delta =  -0.19e-3
delta = -0.143e-3
logger.info('Global delta: %s' % delta)

## //down
## if (run >=174500   && run <= 175100 ) alpha = 0;
## else if (run >= 175101  && run <= 175900 )  alpha =0.0302171e-3 ;
## else if (run >= 175901  && run <= 176100 )  alpha =0.0558559e-3 ;
## else if (run >= 176101  && run <= 176330 )  alpha = 0.0874364e-3;
## //up
## else if (run >= 176657 && run <= 177000) alpha = 0.130490e-3;
## else if (run >= 177001 && run <= 177250) alpha = 0.146611e-3;
## else if (run >= 177251 && run <= 177350) alpha =  0.166163e-3;
## else if (run >= 177351 && run <= 177500) alpha =  0.153974e-3;
## //
## else if (run >= 177501 && run <= 177600) alpha =  0.239742e-3;
## else if (run >= 177601 && run <= 177800) alpha = 0.299172e-3;
## //
## else if (run >= 178001 && run <= 178250) alpha = 0.238894e-3;
## else if (run >= 178251 && run <= 178500) alpha = 0.221683e-3;
## else if (run >= 178501 && run <= 179001) alpha = 0.232447e-3;
## else if (run >= 179001 && run <= 179500) alpha = 0.251556e-3;
## else if (run >= 179501 && run <= 179999) alpha = 0.246533e-3;
## else if (run >= 180000 && run <= 180300) alpha = 0.270993e-3;
## else if (run >= 180301 && run <= 180640) alpha = 0.255366e-3;
## // down
## else if (run >= 181200  && run <= 181700 )  alpha = 0.0686362e-3 ;
## else if (run >= 181700  && run <= 182300 )  alpha =0.0945660e-3;
## else if (run >= 182301  && run <= 182500 )  alpha =0.0683000e-3 ;
## else if (run >= 182501  && run <= 182800 )  alpha = 0.0877048e-3;
## else if (run >= 182801  && run <= 183000 )  alpha = 0.126342e-3;
## else if (run >= 183001  && run <= 183200 )  alpha = 0.119248e-3;
## //up
## else if (run >= 183800  && run <= 184200 )  alpha =0.222877e-3 ;
## else if (run >= 184201  && run <= 184657 )  alpha = 0.211130e-3;
## // down
## else if (run >= 184700  && run <= 185000 )  alpha =0.2111e-3 ;
## else if (run >= 185000  && run <= 185100 )  alpha =0.05129e-3 ;
## else if (run >= 185101  && run <= 185200 )  alpha = -0.00853040e-3 ;
## else if (run >= 185201  && run <= 185300 )  alpha =0.0154273e-3 ;
## else if (run >= 185301  && run <= 185400 )  alpha =0.0351537e-3 ;
## else if (run >= 185401  && run <= 185600 )  alpha = 0.0677423e-3 ;

## Run-dependent offsets
offsets = [
    ## start stop offset
    ## //down
    (175101, 175900, 0.0302171e-3),
    (175901, 176100, 0.0558559e-3),
    (176101, 176330, 0.0874364e-3),
    ## //up
    (176657, 177000, 0.130490e-3),
    (177001, 177250, 0.146611e-3),
    (177251, 177350, 0.166163e-3),
    (177351, 177500, 0.153974e-3),
    ## //
    (177501, 177600, 0.239742e-3),
    (177601, 177800, 0.299172e-3),
    ## //
    (178001, 178250, 0.238894e-3),
    (178251, 178500, 0.221683e-3),
    (178501, 179001, 0.232447e-3),
    (179001, 179500, 0.251556e-3),
    (179501, 179999, 0.246533e-3),
    (180000, 180300, 0.270993e-3),
    (180301, 180640, 0.255366e-3),
    ## // down
    (181200, 181700, 0.0686362e-3),
    (181700, 182300, 0.0945660e-3),
    (182301, 182500, 0.0683000e-3),
    (182501, 182800, 0.0877048e-3),
    (182801, 183000, 0.126342e-3),
    (183001, 183200, 0.119248e-3),
    ## //up
    (183800, 184200, 0.222877e-3),
    (184201, 184657, 0.211130e-3),
    ## // down
    (184700, 185000, 0.2111e-3),
    (185000, 185100, 0.05129e-3),
    (185101, 185200, -0.00853040e-3),
    (185201, 185300, 0.0154273e-3),
    (185301, 185400, 0.0351537e-3),
    (185401, 185600, 0.0677423e-3)
]

# =============================================================================
# Build XML-document
# =============================================================================
buildXML(
    year='2k+16',
    reco='Reco16',
    idp_plus=idp_plus,
    idp_minus=idp_minus,
    offsets=offsets,
    delta=delta)

# ============================================================================
# The END
# ============================================================================
