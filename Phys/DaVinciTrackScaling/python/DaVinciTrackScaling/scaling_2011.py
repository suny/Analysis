#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file scaling_2011.py
#
# Momentum scaling for 2011
#
# Input data are specified close to the start of the script:
#
#  - input ROOT file with historams
#     the file should have two 2D-histograms, named as 'idp-plus' and 'idp-minus'
#
#  - global delta
#  - list of run-dependent offsets as list of triplets:  [ (start,stop,offset) ],
#     where (start,stop) represent the run range (both edges are inclusive)
#
#  As the output xml-file MomentumScale.xml is generated in cwd
#
# @author Matt  NEEDHAM
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date   2013-04-20
# @see TrackScaleState
# ============================================================================
"""Momentum sclaing for 2011
- input ROOT file with historams
the file should have two 2D-histograms, named as 'idp-plus' and 'idp-minus'

- global delta
- list of run-dependent offsets as list of triplets:  [ (start,stop,offset) ],
where (start,stop) represent the run range (both edges are inclusive)

As the output xml-file MomentumScale.xml is generated in cwd

"""
# ============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2013-04-20"
# ============================================================================
import ROOT, Ostap.PyRoUts
# ============================================================================
from DaVinciTrackScaling.momentum_scale import buildXML, logger
# ============================================================================
logger.info(100 * '*')
logger.info(__doc__)
logger.info(100 * '*')
# ============================================================================

# ============================================================================
## the histograms
with ROOT.TFile('BFitResults-2011repro.root', 'READ') as the_file:
    idp_plus = the_file.Get('idp-plus').clone()
    idp_minus = the_file.Get('idp-minus').clone()
    logger.info('HISTOS are read from %s' % the_file)

# =============================================================================
## global delta
delta = 1.0e-4
logger.info('Global delta: %s' % delta)

# =============================================================================
## CalScale.h:
##   if (run >= 87219  && run <=  89777 ) alpha =0.0;
##   else if (run >= 89333  && run <=  90256 ) alpha = 6.36699e-05  ;
##   else if (run >= 90257  && run <=   90763  ) alpha = 7.11719e-05 ;
##   else if (run >= 91556 && run <= 93282)  alpha = 1.53674e-05 ;
##   else if (run >= 93398  && run <=  94386 ) alpha = 0.000144135 ;
##   else if (run >= 95948  && run <=  97028 ) alpha = 0.000214408  ;
##   else if (run >= 97114  && run <=  98882 ) alpha = 3.41493e-05 ;
##   else if (run >= 98900  && run <=  101862 ) alpha =  0.000137622;
##   else if (run >= 101891  && run <=  102452 ) alpha = 3.73981e-05 ;
##   else if (run >= 102499  && run <=  102907 ) alpha = 0.000169023 ;
##   else if (run >= 103049  && run <=  103687 ) alpha =  3.20303e-05 ;
##   else if (run >= 103954  && run <=  104414 ) alpha =  0.00017584 ;

offsets = [
    ## start   stop     offset
    (87219, 89777, 0.0),
    (89333, 90256, 6.36699e-05),
    (90257, 90763, 7.11719e-05),
    (91556, 93282, 1.53674e-05),
    (93398, 94386, 0.000144135),
    (95948, 97028, 0.000214408),
    (97114, 98882, 3.41493e-05),
    (98900, 101862, 0.000137622),
    (101891, 102452, 3.73981e-05),
    (102499, 102907, 0.000169023),
    (103049, 103687, 3.20303e-05),
    (103954, 104414, 0.00017584),
]

# =============================================================================
# Build XML-document
# =============================================================================
buildXML(
    year='2k+11',
    reco='Reco14',
    idp_plus=idp_plus,
    idp_minus=idp_minus,
    offsets=offsets,
    delta=delta)

# =============================================================================
# The END
# =============================================================================
