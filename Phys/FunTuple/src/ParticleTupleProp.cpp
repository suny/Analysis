/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "ParticleTupleProp.h"

bool ParticleTupleProp::checks() {
  if ( m_branchname.empty() ) { return ErrorMsg( "Error branch name is empty" ); }

  if ( m_decaydescriptor.empty() ) { return ErrorMsg( "Error decay descriptor is empty" ); }

  int count( 0 );
  for ( const auto& mtp : m_tupleprops ) {
    if ( mtp.Func.empty() ) { return ErrorMsg( "Functor name is empty at " + std::to_string( count ) ); }
    if ( mtp.Func_branchname.empty() ) {
      return ErrorMsg( "Functor branchname is empty at " + std::to_string( count ) );
    }
    if ( mtp.Func_returntype.empty() ) {
      return ErrorMsg( "Functor return type is empty at " + std::to_string( count ) );
    }
    count++;
  }
  return true;
}

void ParticleTupleProp::setFunctorProps( std::vector<std::string>& functors, std::vector<std::string>& funcbranchnames,
                                         std::vector<std::string>& funcreturntypes ) {
  for ( unsigned int i = 0; i < functors.size(); i++ ) {
    ParticleProp::FunctorProp tupleprop;
    tupleprop.Func            = functors[i];
    tupleprop.Func_branchname = funcbranchnames[i];
    tupleprop.Func_returntype = funcreturntypes[i];
    m_tupleprops.push_back( tupleprop );
  }
}

bool ParticleTupleProp::ErrorMsg( std::string stg ) { // TODO: better exception handling
  std::cout << stg << std::endl;
  return false;
}
