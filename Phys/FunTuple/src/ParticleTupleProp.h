/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// std lib
#include <iostream>
#include <vector>

// LoKi
#include "LoKi/IDecay.h"
#include "LoKi/IHybridFactory.h"
#include "LoKi/IMCDecay.h"
#include "LoKi/IMCHybridFactory.h"
#include "LoKi/Trees.h"

namespace ParticleProp {
  typedef std::vector<std::vector<std::string>> TUPLELIST;
  // define a struct to hold functor tupling properties
  struct FunctorProp {
    std::string Func{""};            // Functor name
    std::string Func_branchname{""}; // Functor TBranch name
    std::string Func_returntype{""}; // Functor return type
  };
} // namespace ParticleProp

class ParticleTupleProp {
public:
  ParticleTupleProp() {}
  // set functions
  void setBranchName( std::string branchname ) { m_branchname = branchname; }
  void setDecayDescriptor( std::string decaydescriptor ) { m_decaydescriptor = decaydescriptor; }
  void setFunctorProps( std::vector<ParticleProp::FunctorProp> tupleprops ) { m_tupleprops = tupleprops; }
  void setFunctorProps( std::vector<std::string>& functors, std::vector<std::string>& funcbranchnames,
                        std::vector<std::string>& funcreturntypes );
  // get functions
  const std::string&                            BranchName() const { return m_branchname; }
  const std::string&                            DecayDescriptor() const { return m_decaydescriptor; }
  const std::vector<ParticleProp::FunctorProp>& FunctorProps() const { return m_tupleprops; }
  // check and validate
  bool checks();

protected:
  bool ErrorMsg( std::string stg ); // TODO: Better exception handling
private:
  std::string                            m_branchname{""};
  std::string                            m_decaydescriptor{""};
  std::vector<ParticleProp::FunctorProp> m_tupleprops;
};
