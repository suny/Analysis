/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/Vector4DTypes.h"

// Event
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/Particle.h"

// LoKi
#include "LoKi/IDecay.h"
#include "LoKi/IHybridFactory.h"
#include "LoKi/IMCDecay.h"
#include "LoKi/IMCHybridFactory.h"
#include "LoKi/Trees.h"

// Kernel
#include "Kernel/IDecayFinder.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleID.h"
#include "Kernel/ParticleProperty.h"

// boost
#include <boost/algorithm/string/join.hpp>

// custom class
#include "ParticleTupleProp.h"

namespace LHCb {
  namespace FTuple {
    template <class T>
    using Consumer =
        Gaudi::Functional::Consumer<void( const T& ), Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>>;
    typedef std::vector<std::string> TUPLE;
    typedef std::vector<TUPLE>       TUPLELIST;
    typedef std::vector<MCParticle>  MCPARTICLES;
    typedef std::vector<Particle>    PARTICLES;
  } // namespace FTuple
} // namespace LHCb

template <class T>
class FunTuple : public LHCb::FTuple::Consumer<T> {
public:
  FunTuple( const std::string& name, ISvcLocator* pSvc );
  virtual StatusCode initialize() override;
  virtual void       operator()( const T& particles ) const override;

protected:
  template <typename T2>
  StatusCode booktuple( const Tuples::Tuple& m_ntuple, const std::string& colname, const T2& val ) const;
  StatusCode checks();
  StatusCode setParticleTupleProps();
  void       FindAndBookTuple( const unsigned int& i, const T& particles, const Tuples::Tuple& ntuple ) const {
    // variables used in explicit template specialisation, so suppress unused variable compiler warning here
    (void)i;
    (void)particles;
    (void)ntuple;
  }
  //(Will be removed): The following overloaded function (makeMCTwoBodyDecay) are only used when m_makeCustomData is set
  // to true (see m_makeCustomData property description for more info)
  void makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2, const int& parentid ) const;
  void makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2, const int& parentid,
                           const int& prod1id, const int& prod2id, MsgStream& log ) const;

private:
  Gaudi::Property<std::string> m_ntupleTname{this, "tree_name", "DecayTree", "Set TTree name"};
  Gaudi::Property<bool>        m_isMC{this, "is_MC", true, "Set is MC TRUTH or not"};
  // After defining custom Gaudi property remove m_branchnames, m_decaydescriptors, m_functors, m_funcbranchnames,
  // m_funcreturntypes  (Do we need the custom Gaudi::Property like  ParticleTupleProp?)
  Gaudi::Property<LHCb::FTuple::TUPLE>     m_branchnames{this, "branch_names", LHCb::FTuple::TUPLE(),
                                                     "List of TBranch names"};
  Gaudi::Property<LHCb::FTuple::TUPLE>     m_decaydescriptors{this, "decay_descriptors", LHCb::FTuple::TUPLE(),
                                                          "List of DecayDescriptors"};
  Gaudi::Property<LHCb::FTuple::TUPLELIST> m_functors{this, "functors", LHCb::FTuple::TUPLELIST(),
                                                      "List of functor names"};
  Gaudi::Property<LHCb::FTuple::TUPLELIST> m_funcbranchnames{this, "functor_branch_names", LHCb::FTuple::TUPLELIST(),
                                                             "List of functor TBranch names"};
  // TODO: Could m_funcreturntypes propery handle the user requested precision on the variables being tupled?
  Gaudi::Property<LHCb::FTuple::TUPLELIST>  m_funcreturntypes{this, "functor_return_types", LHCb::FTuple::TUPLELIST(),
                                                             "List of functor return types"};
  Gaudi::Property<std::vector<std::string>> m_preamble_def{this, "Preamble", {}, "List of preamble strings"};
  Gaudi::Property<bool>                     m_makeCustomData{
      this, "make_custom_data", true,
      "(Property for testing only, will be removed) Should only be set to true if you ran over MakeData algorithm before hand. \
												 If set to true, makes Bs->Jpsi(mumu)phi(KK)\
												 Feature introduced because when Bs head particle is built in MakeData algorithm and put into TES, and then is passed into this algorithm \
												 the products of Bs are garbage. Turns out TES is not clever enough to take ownership 'recursively'"};
  // vector of ParticleTupleProp custom objects. These are built out of above member variables (this is the object I
  // want the Gaudi::Property to be?)
  std::vector<ParticleTupleProp> m_particletupleprops;
  // for Particle Loki functors
  std::string                                m_preamble;
  ToolHandle<LoKi::IHybridFactory>           m_factory = {"LoKi::Hybrid::Tool/HybridFactory:PUBLIC", this};
  LoKi::Types::Fun                           m_fun{LoKi::Constant<const LHCb::Particle*, double>( -1.0e+10 )};
  std::vector<std::vector<LoKi::Types::Fun>> m_vfun;
  // for Particle decay descriptor matching
  ToolHandle<Decays::IDecay> m_decaytool = {this, "Decay", "LoKi::Decay"};
  Decays::IDecay::Tree       m_decayTree = Decays::Trees::Invalid_<const LHCb::Particle*>();
  // for MCParticle Loki functors
  ToolHandle<LoKi::IMCHybridFactory>           m_mcfactory = {"LoKi::Hybrid::MCTool/MCHybridFactory:PUBLIC", this};
  LoKi::Types::MCFun                           m_mcfun{LoKi::Constant<const LHCb::MCParticle*, double>( -1.0e+10 )};
  std::vector<std::vector<LoKi::Types::MCFun>> m_vmcfun;
  // for MCParticle decay descriptor matching
  ToolHandle<Decays::IMCDecay> m_mcdecaytool = {this, "MCDecay", "LoKi::MCDecay"};
  Decays::IMCDecay::Tree       m_mcdecayTree = Decays::Trees::Invalid_<const LHCb::MCParticle*>();
  // property service
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{this, "ParticleProperty",
                                                                  "LHCb::ParticlePropertySvc"};
};

template <class T>
FunTuple<T>::FunTuple( const std::string& name, ISvcLocator* pSvc )
    : LHCb::FTuple::Consumer<T>( name, pSvc, {"input_location", {"MyParticle"}} ) {}

template <class T>
StatusCode FunTuple<T>::initialize() {

  // initialise consumer
  StatusCode sc_init = LHCb::FTuple::Consumer<T>::initialize();
  if ( sc_init.isFailure() ) { return this->Error( "Error in initialisation of consumer" ); }

  // conduct checks to see if the parsed arguments are ok
  StatusCode sc_check = checks();
  if ( sc_check.isFailure() ) { return this->Error( "Error in checks" ); }

  // Using the arguments make a list of ParticleTupleProp objects that hold info on (TBranch name, Decay descriptor,
  // List of FunctorProp objects). The FunctorProp objects hold info on (functor name, functor TBranch name, functor
  // return type (can handle precision?))
  StatusCode sc_ptp = setParticleTupleProps();
  if ( sc_ptp.isFailure() ) { return this->Error( "Error in setting ParticleTupleProps" ); }

  // Instantiate all the functors (unfortunately the FunctorProp objects created in the previous step could not hold
  // this information).
  m_preamble = boost::algorithm::join( m_preamble_def.value(), "\n" );
  for ( const auto& ptp : m_particletupleprops ) {
    std::vector<LoKi::Types::MCFun> imfunc_mc;
    std::vector<LoKi::Types::Fun>   imfunc;
    for ( const auto& tup : ptp.FunctorProps() ) {
      if ( m_isMC ) { // MC Particles
        StatusCode sc_func_mc = m_mcfactory->get( tup.Func, m_mcfun, m_preamble );
        if ( sc_func_mc.isFailure() ) {
          return this->Error( "Error in MC instantiating functor in particle branch " + ptp.BranchName() +
                              " with functor branch name " + tup.Func_branchname );
        }
        imfunc_mc.push_back( m_mcfun );
        // this->info() << "Created a function in " + ptp.BranchName() + " with " + tup.Func_branchname + " : The
        // function is '" << imfunc_mc.back() << "'" << endmsg;
      } else { // Particles
        StatusCode sc_func = m_factory->get( tup.Func, m_fun, m_preamble );
        if ( sc_func.isFailure() ) {
          return this->Error( "Error in instantiating functor in particle branch " + ptp.BranchName() +
                              " with functor branch name " + tup.Func_branchname );
        }
        imfunc.push_back( m_fun );
        // this->info() << "Created a function in " + ptp.BranchName() + " with " + tup.Func_branchname + " : The
        // function is '" << imfunc.back() << "'" << endmsg;
      }
    }
    if ( m_isMC ) { // MC Particles related
      m_vmcfun.push_back( imfunc_mc );
    } else { // Particles related
      m_vfun.push_back( imfunc );
    }
  }
  return StatusCode::SUCCESS;
}

template <class T>
StatusCode FunTuple<T>::checks() {
  // check tuple name
  if ( m_ntupleTname.empty() ) { return this->Error( "Error tree name is empty" ); }

  // check related to containers
  if ( m_branchnames.empty() ) { return this->Error( "Error branch names container is empty" ); }
  if ( m_decaydescriptors.empty() ) { return this->Error( "Error decay descriptor container is empty" ); }
  if ( m_functors.empty() ) { return this->Error( "Error functor container is empty" ); }
  if ( m_funcbranchnames.empty() ) { return this->Error( "Error functor branch names container is empty" ); }
  if ( m_funcreturntypes.empty() ) { return this->Error( "Error functor return types container is empty" ); }
  if ( m_branchnames.size() != m_decaydescriptors.size() ) {
    return this->Error( "Error size of branch names and decay descriptors containers do not match" );
  }
  if ( m_branchnames.size() != m_functors.size() ) {
    return this->Error( "Error size of branch names and functor lists containers do not match" );
  }
  if ( m_branchnames.size() != m_funcbranchnames.size() ) {
    return this->Error( "Error size of branch names and functor branch names containers do not match" );
  }
  if ( m_branchnames.size() != m_funcreturntypes.size() ) {
    return this->Error( "Error size of branch names and functor return types containers do not match" );
  }

  return StatusCode::SUCCESS;
}

template <class T>
StatusCode FunTuple<T>::setParticleTupleProps() {
  // make a list of ParticleTupleProp objects to hold info related to particle being tupled
  for ( unsigned int i = 0; i < m_branchnames.size(); i++ ) {
    ParticleTupleProp ptp = ParticleTupleProp();
    ptp.setBranchName( m_branchnames[i] );
    ptp.setDecayDescriptor( m_decaydescriptors[i] );
    // make a list of FunctorProp objects to hold functor properties
    ptp.setFunctorProps( m_functors[i], m_funcbranchnames[i], m_funcreturntypes[i] );
    // conduct checks on whether input are empty or not
    if ( !ptp.checks() ) { return this->Error( "Error conducting checks with ParticleTupleProp" ); }
    m_particletupleprops.push_back( ptp );
  }

  return StatusCode::SUCCESS;
}

template <class T>
template <typename T2>
StatusCode FunTuple<T>::booktuple( const Tuples::Tuple& ntuple, const std::string& colname, const T2& val ) const {
  return ntuple->column( colname, val );
}

template <class T>
void FunTuple<T>::operator()( const T& particles ) const {
  // make ntuple
  Tuples::Tuple ntuple = this->nTuple( m_ntupleTname );

  // loop over ParticleTupleProp (i.e. individual decay descriptors to get the particles)
  for ( unsigned int i = 0; i < m_particletupleprops.size(); i++ ) { FindAndBookTuple( i, particles, ntuple ); }

  // write the tuple
  StatusCode sc_write = ntuple->write();
  if ( sc_write.isFailure() ) { this->err() << "Unable to write the tuple " << endmsg; }
}

template <>
void FunTuple<LHCb::FTuple::MCPARTICLES>::FindAndBookTuple( const unsigned int&              i,
                                                            const LHCb::FTuple::MCPARTICLES& particles,
                                                            const Tuples::Tuple&             ntuple ) const {
  // Messaging service
  MsgStream log = this->info();
  MsgStream err = this->err();

  // check that correct functors related to MCParticles are loaded
  // TODO: MC Truth association with the reco particles needs to be handled
  if ( !m_isMC ) { err << "Have not asked for MC but passed LHCb::MCParticles" << endmsg; }

  // Get the tuple properties
  ParticleTupleProp ptp = m_particletupleprops[i];

  // parse the decay descriptor for MC Truth
  const Decays::IMCDecay::Tree decayTree = m_mcdecaytool->tree( ptp.DecayDescriptor() );
  if ( !decayTree ) { err << "Could not find MC decayTree for " + ptp.DecayDescriptor() << endmsg; }
  log << "MC decay descriptor parsed is " << decayTree << endmsg;

  // create the decay finder  for MC Truth
  Decays::IMCDecay::Finder finder( decayTree );
  if ( !finder ) { err << "Unable to create MC decay finder'" << endmsg; }

  // make input and output constvector
  LHCb::MCParticle::ConstVector output;
  LHCb::MCParticle::ConstVector input;

  // Make new particles (will be removed) only used when m_makeCustomData is True (see m_makeCustomData property
  // description for more info)
  LHCb::MCParticle p1   = LHCb::MCParticle();
  LHCb::MCParticle p1_a = LHCb::MCParticle();
  LHCb::MCParticle p1_b = LHCb::MCParticle();
  LHCb::MCParticle p2   = LHCb::MCParticle();
  LHCb::MCParticle p2_a = LHCb::MCParticle();
  LHCb::MCParticle p2_b = LHCb::MCParticle();
  LHCb::MCParticle p    = LHCb::MCParticle();
  if ( m_makeCustomData ) {
    // set their properties of new particles
    makeMCTwoBodyDecay( p1, p1_a, p1_b, 443, 13, -13, err );   // Jpis -> mu- mu+
    makeMCTwoBodyDecay( p2, p2_a, p2_b, 333, 321, -321, err ); // phi -> K+ K-
    makeMCTwoBodyDecay( p, p1, p2, 531 );                      // Bs -> Jpsi phi
    input.push_back( &p );
  } else {
    // fill the input vector from the input directly
    for ( const auto& p : particles ) { input.push_back( &p ); }
  }

  // find the MC decay
  finder.findDecay( input.begin(), input.end(), output );

  // TODO: Need to handle multiple cands. For now just throw an error
  if ( output.size() > 1 ) { err << "Multiple particles match the decay descriptor " << endmsg; }
  log << "Found #" << output.size() << " decays" << endmsg;
  // for (const auto& p: output){log << *p << endmsg;}

  // loop over FunctorProp to get individual functor outputs
  for ( unsigned int j = 0; j < ( ptp.FunctorProps() ).size(); j++ ) {
    ParticleProp::FunctorProp functup    = ( ptp.FunctorProps() )[j];
    std::string               branchname = ptp.BranchName() + "_" + functup.Func_branchname;
    double                    val        = m_vmcfun[i][j]( output[0] );
    // log << "Had created a function '" << mcfunct << "'" << endmsg;
    // log << "value function '" << val << "'" << endmsg;

    // book tuple
    StatusCode sc_book = booktuple( ntuple, branchname, val );
    if ( sc_book.isFailure() ) { err << "Unable to book the columns " << endmsg; }
  }
}

template <>
void FunTuple<LHCb::FTuple::PARTICLES>::FindAndBookTuple( const unsigned int&            i,
                                                          const LHCb::FTuple::PARTICLES& particles,
                                                          const Tuples::Tuple&           ntuple ) const {
  // Messaging service
  MsgStream log = this->info();
  MsgStream err = this->err();

  // check that correct functors related to Particles are loaded
  // TODO: MC Truth association with the reco particles needs to be handled
  if ( m_isMC ) { err << "Asked for MC but passed LHCb::Particles" << endmsg; }

  // Get the tuple properties
  ParticleTupleProp ptp = m_particletupleprops[i];

  // parse the decay descriptor for reco
  const Decays::IDecay::Tree decayTree = m_decaytool->tree( ptp.DecayDescriptor() );
  if ( !decayTree ) { err << "Could not find decayTree for " + ptp.DecayDescriptor() << endmsg; }
  log << "Decay descriptor parsed is " << decayTree << endmsg;

  // create the decay finder for reco
  Decays::IDecay::Finder finder( decayTree );
  if ( !finder ) { err << "Unable to create decay finder'" << endmsg; }

  // make input and output constvector
  LHCb::Particle::ConstVector output;
  LHCb::Particle::ConstVector input;
  for ( const auto& p : particles ) { input.push_back( &p ); }
  finder.findDecay( input.begin(), input.end(), output );

  // TODO: Need to handle multiple cands. For now just throw an error
  if ( output.size() > 1 ) { this->err() << "Multiple particles match the decay descriptor " << endmsg; }
  this->info() << "Found #" << output.size() << " decays" << endmsg;
  // for (const auto& p: output){log << *p << endmsg;}

  for ( unsigned int j = 0; j < ( ptp.FunctorProps() ).size(); j++ ) {
    ParticleProp::FunctorProp functup    = ( ptp.FunctorProps() )[j];
    std::string               branchname = ptp.BranchName() + "_" + functup.Func_branchname;
    double                    val        = m_vfun[i][j]( output[0] );
    // log << "value function '" << val << "'" << endmsg;

    // book tuple
    StatusCode sc_book = booktuple( ntuple, branchname, val );
    if ( sc_book.isFailure() ) { err << "Unable to book the columns " << endmsg; }
  }
}

// This function is for testing purpose only and will be used only when m_makeCustomData is True, otherwise useless (see
// makeMCTwoBodyDecay property description for more info)
template <class T>
void FunTuple<T>::makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2,
                                      const int& parentid, const int& prod1id, const int& prod2id,
                                      MsgStream& log ) const {
  using std::sqrt;

  // particle id of products
  const LHCb::ParticleID p1_id = LHCb::ParticleID( prod1id ); // mu+
  const LHCb::ParticleID p2_id = LHCb::ParticleID( prod2id ); // mu-
  p1.setParticleID( p1_id );
  p2.setParticleID( p2_id );
  // four momenta of products
  const LHCb::ParticleProperty* p1_prop = m_particlePropertySvc->find( p1_id );
  const LHCb::ParticleProperty* p2_prop = m_particlePropertySvc->find( p2_id );
  if ( !p1_prop || !p2_prop ) { log << "Could not find either p1_prop or p2_prop" << endmsg; }
  const double               p1_mass = p1_prop->mass();
  const double               p2_mass = p2_prop->mass();
  const Gaudi::XYZVector     p1_3vec{10.2, 10.9, 30.2};
  const Gaudi::XYZVector     p2_3vec{20.5, 20.5, 60.6};
  double                     p1_E    = sqrt( p1_3vec.Mag2() + p1_mass * p1_mass );
  double                     p2_E    = sqrt( p2_3vec.Mag2() + p2_mass * p2_mass );
  const Gaudi::LorentzVector p1_4vec = Gaudi::LorentzVector( p1_3vec.X(), p1_3vec.Y(), p1_3vec.Z(), p1_E );
  const Gaudi::LorentzVector p2_4vec = Gaudi::LorentzVector( p2_3vec.X(), p2_3vec.Y(), p2_3vec.Z(), p2_E );
  p1.setMomentum( p1_4vec );
  p2.setMomentum( p2_4vec );

  // Define origin vertex of parent (DecayVertex)
  SmartRef<LHCb::MCVertex> p_orgvertx = new LHCb::MCVertex();
  const Gaudi::XYZPoint    p_orgvrtx{2.8, -1.5, 1.3};
  p_orgvertx->setPosition( p_orgvrtx );
  p_orgvertx->setTime( 0.05 );
  p_orgvertx->setType( LHCb::MCVertex::DecayVertex );

  // Define end vertices of parent
  const Gaudi::XYZPoint            p_endvrtx{6.8, -4.5, 3.3};
  SmartRefVector<LHCb::MCParticle> prods_endvertx;
  prods_endvertx.push_back( &p1 );
  prods_endvertx.push_back( &p2 );
  SmartRef<LHCb::MCVertex> p_endvertx = new LHCb::MCVertex();
  p_endvertx->setPosition( p_endvrtx );
  p_endvertx->setTime( 0.13 );
  p_endvertx->setType( LHCb::MCVertex::DecayVertex );
  p_endvertx->setProducts( prods_endvertx ); // products in this vertex (particle 1 and 2)
  SmartRefVector<LHCb::MCVertex> p_endvertxs;
  p_endvertxs.push_back( p_endvertx );

  // set properties of parent
  const LHCb::ParticleID p_id = LHCb::ParticleID( parentid ); // Jpsi
  p.setParticleID( p_id );
  p.setMomentum( p1_4vec + p2_4vec );
  p.setOriginVertex( p_orgvertx );
  p.setEndVertices( p_endvertxs );

  /*
  this->info() << p << endmsg;
  for (const auto& mcv: p.endVertices()){
          this->info() << *mcv << endmsg;
          for (const auto& mcd: (*mcv).products()){this->info() << *mcd << endmsg;}
  }
  */
}

// This function is for testing purpose only and will be used only when m_makeCustomData is True, otherwise useless (see
// makeMCTwoBodyDecay property description for more info)
template <class T>
void FunTuple<T>::makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2,
                                      const int& parentid ) const {
  // get 4-momenta
  const Gaudi::LorentzVector p1_4vec = p1.momentum();
  const Gaudi::LorentzVector p2_4vec = p2.momentum();

  // Define origin vertex of parent (ppCollision vertex)
  SmartRef<LHCb::MCVertex> p_orgvertx = new LHCb::MCVertex();
  p_orgvertx->setType( LHCb::MCVertex::ppCollision );

  // Define end vertices of parent
  const Gaudi::XYZPoint            p_endvrtx{2.8, -1.5, 1.3};
  SmartRefVector<LHCb::MCParticle> prods_endvertx;
  prods_endvertx.push_back( &p1 );
  prods_endvertx.push_back( &p2 );
  SmartRef<LHCb::MCVertex> p_endvertx = new LHCb::MCVertex();
  p_endvertx->setPosition( p_endvrtx );
  p_endvertx->setTime( 0.05 );
  p_endvertx->setType( LHCb::MCVertex::DecayVertex );
  p_endvertx->setProducts( prods_endvertx ); // products in this vertex (particle 1 and 2)
  SmartRefVector<LHCb::MCVertex> p_endvertxs;
  p_endvertxs.push_back( p_endvertx );

  // set properties of p
  const LHCb::ParticleID p_id = LHCb::ParticleID( parentid ); // Jpsi
  p.setParticleID( p_id );
  p.setMomentum( p1_4vec + p2_4vec );
  p.setOriginVertex( p_orgvertx );
  p.setEndVertices( p_endvertxs );

  /*
  this->info() << p << endmsg;
  for (const auto& mcv: p.endVertices()){
          this->info() << *mcv << endmsg;
          for (const auto& mcd: (*mcv).products()){this->info() << *mcd << endmsg;}
  }
  */
}

DECLARE_COMPONENT_WITH_ID( FunTuple<LHCb::FTuple::MCPARTICLES>, "FunTuple" )
