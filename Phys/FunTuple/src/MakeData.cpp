/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiAlg/Producer.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/Vector4DTypes.h"

// Event
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"

// Kernel
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleID.h"
#include "Kernel/ParticleProperty.h"

// LoKi
#include "LoKi/IMCDecay.h"
#include "LoKi/PrintMCDecay.h"
#include "LoKi/Trees.h"

class MakeData : public Gaudi::Functional::Producer<std::vector<LHCb::MCParticle>()> {
public:
  MakeData( const std::string& name, ISvcLocator* svcLoc )
      : Producer( name, svcLoc, {"output_location", {"MyParticle"}} ) {}
  std::vector<LHCb::MCParticle> operator()() const override;

protected:
  void makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2, const int& parentid,
                           const int& prod1id, const int& prod2id, MsgStream& log ) const;
  void makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2, const int& parentid ) const;
  void checkDecay( Decays::IMCDecay::Finder& finder, LHCb::MCParticle::ConstVector& mcparticles, MsgStream& log ) const;

private:
  Gaudi::Property<std::string> m_headDecay{
      this, "decay_descriptor", "[B_s0 => (J/psi(1S) => mu+ mu- ) ^( phi(1020) => K+ K-)]CC", "Decay descriptor"};
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{this, "ParticleProperty",
                                                                  "LHCb::ParticlePropertySvc"};
  ToolHandle<Decays::IMCDecay>              m_mcdecaytool = {this, "MCDecay", "LoKi::MCDecay"};
};

DECLARE_COMPONENT( MakeData )

std::vector<LHCb::MCParticle> MakeData::operator()() const {
  MsgStream log = info();
  MsgStream err = error();
  log << "executing MakeData" << endmsg;

  // parse/decode the decay descriptor
  const Decays::IMCDecay::Tree mcdecayTree = m_mcdecaytool->tree( m_headDecay );
  if ( !mcdecayTree ) { err << "Could not find mcdecayTree for " + m_headDecay << endmsg; }
  log << "MC decay descriptor parsed is " << mcdecayTree << endmsg;

  // Define and fill container to hold the Jpsi MCParticles (only one Jpsi made) which decays to mu+ mu-
  std::vector<LHCb::MCParticle> mcparticles;
  LHCb::MCParticle              p1   = LHCb::MCParticle();
  LHCb::MCParticle              p1_a = LHCb::MCParticle();
  LHCb::MCParticle              p1_b = LHCb::MCParticle();
  LHCb::MCParticle              p2   = LHCb::MCParticle();
  LHCb::MCParticle              p2_a = LHCb::MCParticle();
  LHCb::MCParticle              p2_b = LHCb::MCParticle();
  LHCb::MCParticle              p    = LHCb::MCParticle();

  makeMCTwoBodyDecay( p1, p1_a, p1_b, 443, 13, -13, err );   // Jpis -> mu- mu+
  makeMCTwoBodyDecay( p2, p2_a, p2_b, 333, 321, -321, err ); // phi -> K+ K-
  makeMCTwoBodyDecay( p, p1, p2, 531 );                      // Bs -> Jpsi phi

  mcparticles.push_back( p1 );
  mcparticles.push_back( p1_a );
  mcparticles.push_back( p1_b );
  mcparticles.push_back( p2 );
  mcparticles.push_back( p2_a );
  mcparticles.push_back( p2_b );
  mcparticles.push_back( p );

  LHCb::MCParticle::ConstVector mcparticlescheck{&mcparticles[0]};
  /*
  //Check all the particles are there
  for (const auto& mcp: mcparticlescheck) {
          log << *mcp << endmsg;
          for (const auto& mcv: (*mcp).endVertices()){
                  //log << *mcv << endmsg;
                  for (const auto& mcp1: (*mcv).products()){
                          log << *mcp1 << endmsg;
                          for (const auto& mcv1: (*mcp1).endVertices()){
                                  //log << *mcv1 << endmsg;
                                  for (const auto& mcp2: (*mcv1).products()){log << *mcp2 << endmsg;}
                          }
                  }
          }
  }
  */

  // create the decay finder
  Decays::IMCDecay::Finder finder( mcdecayTree );
  if ( !finder ) { err << "Unable to create decay finder'" << endmsg; }

  ////check that the finder can find the decay using the descriptor
  checkDecay( finder, mcparticlescheck, log );

  return mcparticles;
}

void MakeData::makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2, const int& parentid,
                                   const int& prod1id, const int& prod2id, MsgStream& log ) const {
  using std::sqrt;

  // particle id of products
  const LHCb::ParticleID p1_id = LHCb::ParticleID( prod1id ); // mu+
  const LHCb::ParticleID p2_id = LHCb::ParticleID( prod2id ); // mu-
  p1.setParticleID( p1_id );
  p2.setParticleID( p2_id );
  // four momenta of products
  const LHCb::ParticleProperty* p1_prop = m_particlePropertySvc->find( p1_id );
  const LHCb::ParticleProperty* p2_prop = m_particlePropertySvc->find( p2_id );
  if ( !p1_prop || !p2_prop ) { log << "Could not find either p1_prop or p2_prop" << endmsg; }
  const double               p1_mass = p1_prop->mass();
  const double               p2_mass = p2_prop->mass();
  const Gaudi::XYZVector     p1_3vec{10.2, 10.9, 30.2};
  const Gaudi::XYZVector     p2_3vec{20.5, 20.5, 60.6};
  double                     p1_E    = sqrt( p1_3vec.Mag2() + p1_mass * p1_mass );
  double                     p2_E    = sqrt( p2_3vec.Mag2() + p2_mass * p2_mass );
  const Gaudi::LorentzVector p1_4vec = Gaudi::LorentzVector( p1_3vec.X(), p1_3vec.Y(), p1_3vec.Z(), p1_E );
  const Gaudi::LorentzVector p2_4vec = Gaudi::LorentzVector( p2_3vec.X(), p2_3vec.Y(), p2_3vec.Z(), p2_E );
  p1.setMomentum( p1_4vec );
  p2.setMomentum( p2_4vec );

  // Define origin vertex of parent (DecayVertex)
  SmartRef<LHCb::MCVertex> p_orgvertx = new LHCb::MCVertex();
  const Gaudi::XYZPoint    p_orgvrtx{2.8, -1.5, 1.3};
  p_orgvertx->setPosition( p_orgvrtx );
  p_orgvertx->setTime( 0.05 );
  p_orgvertx->setType( LHCb::MCVertex::DecayVertex );

  // Define end vertices of parent
  const Gaudi::XYZPoint            p_endvrtx{6.8, -4.5, 3.3};
  SmartRefVector<LHCb::MCParticle> prods_endvertx;
  prods_endvertx.push_back( &p1 );
  prods_endvertx.push_back( &p2 );
  SmartRef<LHCb::MCVertex> p_endvertx = new LHCb::MCVertex();
  p_endvertx->setPosition( p_endvrtx );
  p_endvertx->setTime( 0.13 );
  p_endvertx->setType( LHCb::MCVertex::DecayVertex );
  p_endvertx->setProducts( prods_endvertx ); // products in this vertex (particle 1 and 2)
  SmartRefVector<LHCb::MCVertex> p_endvertxs;
  p_endvertxs.push_back( p_endvertx );

  // set properties of parent
  const LHCb::ParticleID p_id = LHCb::ParticleID( parentid ); // Jpsi
  p.setParticleID( p_id );
  p.setMomentum( p1_4vec + p2_4vec );
  p.setOriginVertex( p_orgvertx );
  p.setEndVertices( p_endvertxs );

  // log << p << endmsg;
  // for (const auto& mcv: p.endVertices()){
  //	log << *mcv << endmsg;
  //	for (const auto& mcd: (*mcv).products()){log << *mcd << endmsg;}
  //}
}

void MakeData::makeMCTwoBodyDecay( LHCb::MCParticle& p, LHCb::MCParticle& p1, LHCb::MCParticle& p2,
                                   const int& parentid ) const {
  // get 4-momenta
  const Gaudi::LorentzVector p1_4vec = p1.momentum();
  const Gaudi::LorentzVector p2_4vec = p2.momentum();

  // Define origin vertex of parent (ppCollision vertex)
  SmartRef<LHCb::MCVertex> p_orgvertx = new LHCb::MCVertex();
  p_orgvertx->setType( LHCb::MCVertex::ppCollision );

  // Define end vertices of parent
  const Gaudi::XYZPoint            p_endvrtx{2.8, -1.5, 1.3};
  SmartRefVector<LHCb::MCParticle> prods_endvertx;
  prods_endvertx.push_back( &p1 );
  prods_endvertx.push_back( &p2 );
  SmartRef<LHCb::MCVertex> p_endvertx = new LHCb::MCVertex();
  p_endvertx->setPosition( p_endvrtx );
  p_endvertx->setTime( 0.05 );
  p_endvertx->setType( LHCb::MCVertex::DecayVertex );
  p_endvertx->setProducts( prods_endvertx ); // products in this vertex (particle 1 and 2)
  SmartRefVector<LHCb::MCVertex> p_endvertxs;
  p_endvertxs.push_back( p_endvertx );

  // set properties of p
  const LHCb::ParticleID p_id = LHCb::ParticleID( parentid ); // Jpsi
  p.setParticleID( p_id );
  p.setMomentum( p1_4vec + p2_4vec );
  p.setOriginVertex( p_orgvertx );
  p.setEndVertices( p_endvertxs );
}

void MakeData::checkDecay( Decays::IMCDecay::Finder& finder, LHCb::MCParticle::ConstVector& mcparticles,
                           MsgStream& log ) const {
  // make the output container
  typedef LHCb::MCParticle::ConstVector OUTPUT;
  OUTPUT                                output;

  // fill the container
  finder.findDecay( mcparticles.begin(), mcparticles.end(), output );
  log << " found #" << output.size() << " decays" << endmsg;

  // print the marked particle in the container
  for ( const auto& p : output ) {
    log << *p << endmsg;
    // for (const auto& mcv: (*p).endVertices()){
    //	log << *mcv << endmsg;
    //	for (const auto& mcd: (*mcv).products()){log << *mcd << endmsg;}
    //}
  }

  //// print decay: Some reason the loKi services cannot import ParticlePropertyService properly
  // for ( OUTPUT::const_iterator idec = output.begin() ; output.end() != idec ; ++idec )
  //{
  //	const LHCb::MCParticle* dec = *idec ;
  //	if ( 0 == dec ) { continue ; }
  //	log << std::endl << " " << (idec-output.begin()+1) << " \t" ;
  //	LoKi::PrintMC::printDecay(dec,log,true);
  //}
  // log << endmsg ;
}
