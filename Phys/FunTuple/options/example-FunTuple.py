#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example of tupling with the FunTuple algorithm
"""

from Configurables import LHCbApp
from Gaudi.Configuration import ApplicationMgr, NTupleSvc
from GaudiKernel.Configurable import ConfigurableGeneric as RFileCnv
from Configurables import CondDB
from Configurables import MakeData
from Configurables import FunTuple
""" Define helpful class and function

class ParticleTupleProp:
    A class that one can configure with branch_name, decay descriptor and functors.

Function convert_to_parsable_objs:
    Since ParticleTupleProp cannot currently be passed as Gaudi::Property, define
    this function to turn member variables into parsable_objs for FunTuple

"""


class ParticleTupleProp:
    def __init__(self, branch_name, decay_descriptor, particle_code):
        """
        branch_name     : Branch name of particle
        decay_descriptor: Decay descriptor
        particle_code   : List of tuples. Each tuple contains strings defining
                         (Functorcode, corresponding branch name, C++ return type that could handle precision, description).
        """
        self.branch_name = branch_name
        self.decay_descriptor = decay_descriptor
        self.particle_code = particle_code

    def to_dict(self):
        return {self.branch_name: {self.decay_descriptor: self.particle_code}}


def convert_to_parsable_objs(particle_tuple_props):
    """
    particle_tuple_props: List of ParticleTupleProp
    """

    def get_lists(ptps, i):
        f_list = []
        for ptp in ptps:
            f_list += [[pc[i] for pc in ptp.particle_code]]

        return f_list

    branch_list = [ptp.branch_name for ptp in particle_tuple_props]
    discrp_list = [ptp.decay_descriptor for ptp in particle_tuple_props]
    func_list = get_lists(particle_tuple_props, 0)
    func_bnamelist = get_lists(particle_tuple_props, 1)
    func_rtypelist = get_lists(particle_tuple_props, 2)

    return branch_list, discrp_list, func_list, func_bnamelist, func_rtypelist


#Configure LHCb application
app = LHCbApp()
app.DataType = "Upgrade"
app.DDDBtag = "dddb-20171126"
app.CondDBtag = "sim-20171127-vc-md100"
app.Simulation = True
ApplicationMgr().EvtMax = 5
ApplicationMgr().EvtSel = "NONE"
CondDB(Upgrade=True)

#Make MCParticles (just one Bs): Turns out TES is not clever enough to take ownership 'recursively' (set makeCustomData = True if running this algorithm)
algMakeData = MakeData(
    name="Alg_MakeData",
    decay_descriptor=
    "[B_s0 => (J/psi(1S) => mu+ mu- ) ( phi(1020) => K+ K-)]CC",
    output_location="MCParticle")

#Configure Particles Bs and phi
ParticleBs = ParticleTupleProp(
    branch_name="Bs",
    decay_descriptor=
    "[B_s0 => (J/psi(1S) => mu+ mu- ) ( phi(1020) => K+ K-)]CC",
    #List of tuple, each tuple (functor, branch name, precision (TODO), description).
    particle_code=[('MCP', 'TRUEP', 'double', 'True Moment'),
                   ('MCPT', 'TRUEPT', 'double', 'True Transverse moment')])
ParticlePhi = ParticleTupleProp(
    branch_name="Phi",
    decay_descriptor=
    "[B_s0 => (J/psi(1S) => mu+ mu- ) ^( phi(1020) => K+ K-)]CC",
    particle_code=[('MCP', 'TRUEP', 'double', 'True Moment'),
                   ('MCPT', 'TRUEPT', 'double', 'True Transverse moment')])

#Do not need to do this if a custom gaudi property is implemented
branchlist, decaydiscrpList, funcList, funcBranchNameList, funcRetunTypeList = convert_to_parsable_objs(
    [ParticleBs, ParticlePhi])

#Configure FunTuple algorithm
algtupledata = FunTuple(
    name="FunTuple",
    tree_name="DecayTree",
    branch_names=branchlist,
    is_MC=True,  #Since passing MCParticles
    decay_descriptors=decaydiscrpList,
    functors=funcList,
    functor_branch_names=funcBranchNameList,
    functor_return_types=funcRetunTypeList,
    make_custom_data=
    True,  #Only set to True since running over MakeData algo (produces its own Bs->Jpsiphi MC decays).
    input_location="MCParticle",
    NTupleLUN='tuple')

#add the algorithms to the sequence
ApplicationMgr().TopAlg = [algMakeData, algtupledata]

#set Tuple properties
outputname = "FunTuple.root"
NTupleSvc().Output = [
    "{} DATAFILE='{}' TYP='ROOT' OPT='NEW'".format(algtupledata.NTupleLUN,
                                                   outputname)
]
ApplicationMgr().ExtSvc.append(NTupleSvc())
ApplicationMgr().HistogramPersistency = "ROOT"
# Set the compression level for the ROOT tuple file
RFileCnv('RFileCnv').GlobalCompression = "LZMA:6"
